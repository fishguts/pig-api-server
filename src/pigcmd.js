#!/usr/bin/env node

/**
 * User: curtis
 * Date: 05/27/18
 * Time: 1:22 PM
 * Copyright @2018 by Xraymen Inc.
 *
 * A command line utility for doing the things that need to be done
 */

const cli=require("pig-cmd").cli;
const pigcmd=require("./command/pigcmd");
const constant=require("./common/constant");
const {SetupEnvironmentCommand}=require("./command/setup");

cli.run(pigcmd, {
	setupCommand: new SetupEnvironmentCommand({
		mode: constant.application.mode.CLI
	})
});
