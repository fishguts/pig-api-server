/**
 * User: curtis
 * Date: 6/3/18
 * Time: 1:37 AM
 * Copyright @2018 by Xraymen Inc.
 */

const async=require("async");
const {spawn}=require("child_process");
const eventStream=require("event-stream");
const {PigError}=require("pig-core").error;
const http=require("pig-core").http;

/* eslint-disable valid-jsdoc */
/**
 * Does a ps with options you specify.
 * I started with this guys work: https://github.com/indexzero/ps-tree.
 * @param {Array<string>} params
 * @param {Number} parentPID - only find children of this process id
 * @param {Function} callback
 */
exports.ps=function({
	params=["-A", "-o"],
	parentPID=null
}, callback) {
	let headers;
	const processLister=spawn("ps", params.concat("comm, ppid, pid, stat"));
	eventStream.connect(
		processLister.stdout,
		eventStream.split(),
		eventStream.map(function(line, done) {
			// this could parse a lot of unix command output
			const columns=line.trim().split(/\s+/);
			if(columns.length===1) {
				done();
			} else if(!headers) {
				headers=columns.map(column=>column.toLowerCase());
				done();
			} else {
				const row=headers.reduce((result, name, index)=>{
					result[name]=columns[index];
					return result;
				}, {});
				row.pid=Number(row.pid);
				row.ppid=Number(row.ppid);
				done(null, row);
			}
		}),
		eventStream.writeArray(function(error, processes) {
			if(!parentPID) {
				callback(error, processes);
			} else {
				const parents={
						[parentPID]: true
					},
					children=[];
				processes.forEach(function(process) {
					if(parents.hasOwnProperty(process.ppid)) {
						parents[process.pid]=true;
						children.push(process);
					}
				});
				callback(null, children);
			}
		})
	).on("error", callback);
};

/**
 * Finds process id or returns a 404 error...or an error if one occurs
 * @param {Number} pid
 * @param {Function} callback
 */
exports.find=function(pid, callback) {
	async.waterfall([
		exports.ps.bind(null, {}),
		function(processes, done) {
			const process=processes.find((process)=>process.pid===pid);
			if(process) {
				done(null, process);
			} else {
				callback(new PigError({
					message: `process.find(${pid}) failed`,
					statusCode: http.status.code.NOT_FOUND
				}));
			}
		}
	], callback);
};

/**
 * WIll kill the process if it finds it. And will throw an exception if it does not.
 * @param {Number} pid
 * @param {Function} callback
 */
exports.kill=function(pid, callback) {
	try {
		process.kill(pid);
		process.nextTick(callback);
	} catch(error) {
		callback(new PigError({
			error: error,
			message: `process.kill(${pid}) failed`,
			statusCode: http.status.code.NOT_FOUND
		}));
	}
};
