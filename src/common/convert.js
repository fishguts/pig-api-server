/**
 * User: curtis
 * Date: 8/8/18
 * Time: 9:17 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const assert=require("pig-core").assert;
const constant=require("./constant");

/**
 * Convert a string value to the specified type[s]
 * @param {*} dfault - return value if <param>value</param> is "empty"
 * @param {PigDefaultEncoding} encoding
 * @param {string|undefined} value
 * @param {string} type
 * @returns {*|undefined}
 */
exports.fromString=function({value, primaryType,
	dfault=undefined,
	subType=undefined,
	encoding="none"
}) {
	function _split(regex) {
		// we know that value is not null otherwise we would never have gotten here.
		// But an empty string for us equates to nothing specified.
		if(value.length===0) {
			return dfault;
		} else {
			return value.split(regex)
				.map((element)=>exports.fromString({
					dfault: element,
					primaryType, subType,
					value: element
				}));
		}
	}

	if(value==null) {
		return dfault;
	}
	if(encoding==="space-delimited") {
		return _split(/\s+/);
	} else if(encoding==="comma-delimited") {
		return _split(/\s*,\s*/);
	} else if(encoding==="semi-delimited") {
		return _split(/\s*;\s*/);
	} else if(encoding==="punctuation-delimited") {
		return _split(/\s*[-_|,;:]\s*/);
	} else {
		assert.toLog(encoding==="none" || encoding==="string", `encoding=${primaryType}`);
		if(primaryType===constant.project.field.type.STRING) {
			return value;
		} else if(value.length===0) {
			// The reason we return undefined here is because we consider an empty string for all but type=="string" to be undefined.
			return undefined;
		} else {
			if(primaryType===constant.project.field.type.BOOLEAN) {
				return value.match(/^t|1/i)!==null;
			}
			if(primaryType===constant.project.field.type.INTEGER) {
				return _.toInteger(value);
			}
			if(primaryType===constant.project.field.type.NUMBER) {
				return _.toNumber(value);
			}
			if(primaryType===constant.project.field.type.ARRAY) {
				// let's assume that it is separated by commas
				return value.split(/\s*,\s*/)
					.map((element)=>exports.fromString({
						primaryType: subType,
						value: element
					}));
			}
			// Add other conversions as needed.
			return value;
		}
	}
};

