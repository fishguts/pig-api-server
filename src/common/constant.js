/**
 * User: curt
 * Date: 05/27/18
 * Time: 4:30 PM
 */

const constant=require("pig-core").constant;

/**
 * @type {module:pig-core/constant}
 */
module.exports=Object.assign(module.exports, constant);

/**
 * Database constants
 */
exports.database={
	type: {
		MONGO: "mongo"
	}
};

/**
 * Module names in our environment
 */
exports.module={
	MONGO: "mongo",
	SERVER: "server"
};
exports.isValidModule=(value)=>constant.isValidValue(exports.module, value);


/**
 * Known for execution and testing
 */
exports.nodenv={
	TEST: "test",
	LOCAL: "local",
	DEVELOPMENT: "development",
	STAGING: "staging",
	PRODUCTION: "production"
};
exports.isValidNodenv=(value)=>constant.isValidValue(exports.nodenv, value);


/**
 * These are constants that apply to Projects (not our "project")
 */
exports.project={
	/**
	 * Well-known and expected environments
	 */
	env: {
		// This is our own, internal well-known environment. All others are controlled by the author.
		PIG: "pig"
	},
	field: {
		attribute: {
			AUTO_ID: "auto-id",
			MIXED: "mixed",
			REQUIRED: "required"
		},
		propertyOf: {
			// a field found in a model.
			MODEL: "model",
			// A type field that applies to queries.
			QUERY: "query",
			// Describes a response property that is not a field in the model such as "count" or "deleted"
			RESULT: "result"
		},
		type: {
			ARRAY: "array",
			BOOLEAN: "boolean",
			DATE: "date",
			ENUM: "enum",
			ID: "id",
			INTEGER: "integer",
			OBJECT: "object",
			NUMBER: "number",
			STRING: "string"
		}
	},
	request: {
		function: {
			COUNT: "count",
			CREATE_ONE: "create-one",
			CREATE_MANY: "create-many",
			UPSERT_ONE: "upsert-one",
			DELETE_ONE: "delete-one",
			DELETE_MANY: "delete-many",
			DISTINCT: "distinct",
			GET_ONE: "get-one",
			GET_MANY: "get-many",
			UPDATE_ONE: "update-one",
			UPDATE_MANY: "update-many"
		},
		param: {
			function: {
				/**
				 * Fields that filter responses
				 */
				FILTER: "filter",
				/**
				 * Fields that affect the query such as: limit, skip, sort
				 */
				QUERY: "query",
				/**
				 * Fields that carry data for updating the model
				 */
				UPDATE: "update"
			},
			location: {
				BODY: "body",
				HEADER: "header",
				LITERAL: "literal",
				PATH: "path",
				QUERY: "query"
			},
			operation: {
				EQUAL: "eq",
				NOT_EQUAL: "ne",
				GREATER_THAN: "gt",
				GREATER_THAN_EQ: "gte",
				LESS_THAN: "lt",
				LESS_THAN_EQ: "lte",
				UPDATE: "set"
			}
		}
	},
	service: {
		database: {
			CLASS: "database"
		},
		proxy: {
			CLASS: "service",
			type: {
				http: "http"
			}
		},
		transform: {
			CLASS: "service"
		}
	},
	transport: {
		method: {
			DELETE: "delete",
			GET: "get",
			POST: "post",
			PUT: "put"
		}
	}
};


/**
 * status constants
 */
exports.status={
	NONE: null,
	EXECUTE: "execute",
	SUCCESS: "success",
	FAILURE: "failure",
	UNKNOWN: "unknown"
};

/**
 * URN prefix and friends
 */
exports.urn={
	type: {
		PROJECT: "prj",
		QUEUE: "que",
		CONTENT: "cnt",
		ROUTE: "rte",
		database: {
			DOCUMENT: "db:doc",
			KEYVALUE: "db:key"
		}
	}
};
