/**
 * User: curtis
 * Date: 05/27/18
 * Time: 11:48 PM
 * Copyright @2018 by Xraymen Inc.
 */

const http=require("pig-core").http;
const model_factory=require("../data/factory");

/**
 * @type {module:pig-core/http}
 */
module.exports=Object.assign(module.exports, http);

/**
 * Url factories for all routes in our suite.
 */
exports.route={
	diag: {
		/**
		 * Gets ping path for a specified module or the current application
		 * @param {string} module optional. If not specified then defaults to application server
		 * @returns {Url}
		 */
		ping: (module=undefined)=>(module)
			? model_factory.application.get().getModuleServerUrl(module, "diagnostics/ping")
			: model_factory.application.get().getServerUrl("diagnostics/ping")
	},

	project: {
		/**
		 * Gets full Url for the project build service
		 * @param {string} projectId
		 * @returns {Url}
		 */
		projectBuild: (projectId)=>model_factory.application.get().getServerUrl(`/api/project/build/${projectId}`),
		/**
		 * Gets full Url for the create project service
		 * @returns {Url}
		 */
		projectCreate: ()=>model_factory.application.get().getServerUrl("/api/project/create"),
		/**
		 * Gets full Url for the delete project service
		 * @param {string} projectId
		 * @returns {Url}
		 */
		projectDelete: (projectId)=>model_factory.application.get().getServerUrl(`/api/project/delete/${projectId}`),
		/**
		 * Gets full Url for the get project service
		 * @param {string} projectId
		 * @returns {Url}
		 */
		projectGet: (projectId)=>model_factory.application.get().getServerUrl(`/api/project/get/${projectId}`),
		/**
		 * Gets full Url for the get projects service
		 * @returns {Url}
		 */
		projectsGet: ()=>model_factory.application.get().getServerUrl("/api/projects/get"),
		/**
		 * Gets full Url for the project run service
		 * @param {string} projectId
		 * @returns {Url}
		 */
		projectRun: (projectId)=>model_factory.application.get().getServerUrl(`/api/project/run/${projectId}`),
		/**
		 * Gets full Url for the project status service
		 * @param {string} projectId
		 * @returns {Url}
		 */
		projectStatus: (projectId)=>model_factory.application.get().getServerUrl(`/api/project/status/${projectId}`),
		/**
		 * Gets full Url for the project stop execution service
		 * @param {string} projectId
		 * @returns {Url}
		 */
		projectStop: (projectId)=>model_factory.application.get().getServerUrl(`/api/project/stop/${projectId}`),
		/**
		 * Gets full Url for the update project service
		 * @returns {Url}
		 */
		projectUpdate: ()=>model_factory.application.get().getServerUrl("/api/project/update")
	}
};
