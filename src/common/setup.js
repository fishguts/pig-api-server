/**
 * User: curtis
 * Date: 2018-12-06
 * Time: 22:20
 * Copyright @2018 by Xraymen Inc.
 *
 * Configures this applications environment. He is the common companion for <code>command.setup</code>.
 */

const fs=require("fs-extra");
const pig_cmd=require("pig-cmd");
const pig_core=require("pig-core");
const factory=require("../data/factory");

/**
 * Configures this applications environment
 * @param {Application} application
 */
exports.setupEnvironment=function(application=factory.application.get()) {
	function _setupPigCmd() {
		const configuration=pig_cmd.configuration;
		configuration.debug=application.debug.enabled;
		configuration.isLowerEnv=application.isLowerEnv;
		configuration.name=application.name;
		configuration.nodenv=application.nodenv;
		configuration.port=application.server.port;
		configuration.verbose=application.debug.verbose;
	}

	function _setupPigCore() {
		pig_core.log.configure({
			applicationName: application.name,
			configuration: application.log,
			environment: application.nodenv
		});
	}

	/**
	 * Configures all schemas that we consider to be part of a schema "library". The reason
	 * we do this algorithmically is because we library-ify this code (see <link>syncsrc.sh</link>)
	 */
	function _setupSchemaLibrary() {
		try {
			fs.readdirSync("./res/schema")
				.filter(path=>path.endsWith(".json"))
				.forEach(path=>{
					pig_core.validator.addSchema(`./res/schema/${path}`);
				});
		} catch(error) {
			// not all projects have schemas and we assume that this one does not
		}
	}

	_setupPigCore();
	_setupPigCmd();
	_setupSchemaLibrary();
};
