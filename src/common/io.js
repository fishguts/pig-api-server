/**
 * User: curtis
 * Date: 05/27/18
 * Time: 2:28 PM
 * Copyright @2018 by Xraymen Inc.
 */

const {PassThrough}=require("stream");
const assert=require("pig-core").assert;
const log=require("pig-core").log;

/**
 * This guy is a stdio stream capturer.  He supports 0 or more observables. Attach them and let
 * this guy pick up their output
 */
class CaptureStandardIO {
	/**
	 * @param {{stdout:Readable, stderr:Readable}} emitter
	 * @param {function(type:string, line:string)} filter - optional filter by which you may filter or reclassify stderr and stdout
	 * @param {Readable} stdout
	 * @param {Readable} stderr
	 */
	constructor({
		emitter=undefined,
		filter=undefined,
		stdout=undefined,
		stderr=undefined
	}={}) {
		this.filter=filter;
		this._outputBuffer="";
		this._errorBuffer="";
		this._outFn=this._filter.bind(this, "stdout");
		this._errFn=this._filter.bind(this, "stderr");
		this.attach({emitter, stdout, stderr});
	}

	/**
	 * @returns {[{string}]}
	 */
	get err() {
		return CaptureStandardIO._parse(this._errorBuffer);
	}
	/**
	 * @returns {[{string}]}
	 */
	get out() {
		return CaptureStandardIO._parse(this._outputBuffer);
	}

	/**
	 * Attach an observable.
	 * @param {{stdout:Readable, stderr:Readable}} emitter
	 * @param {Readable} stdout
	 * @param {Readable} stderr
	 */
	attach({
		emitter=undefined,
		stdout=undefined,
		stderr=undefined
	}) {
		if(emitter) {
			stdout=emitter.stdout;
			stderr=emitter.stderr;
		}
		if(stdout) {
			stdout.on("data", this._outFn);
		}
		if(stderr) {
			stderr.on("data", this._errFn);
		}
	}

	/**
	 * Detach an already attached observable
	 * @param {{stdout:Readable, stderr:Readable}} emitter
	 * @param {Readable} stdout
	 * @param {Readable} stderr
	 */
	detach({
		emitter=undefined,
		stdout=undefined,
		stderr=undefined
	}) {
		if(emitter) {
			stdout=emitter.stdout;
			stderr=emitter.stderr;
		}
		if(stdout) {
			stdout.removeListener("data", this._outFn);
		}
		if(stderr) {
			stderr.removeListener("data", this._errFn);
		}
	}

	/**
	 * Dumps current state to debug
	 * @param {string} heading
	 */
	dump(heading=undefined) {
		let text=`${this.constructor.name}:`;
		if(heading) {
			text+=` ${heading}:`;
		}
		if(this._errorBuffer.length>0) {
			text+=`\nstderr:\n   ${this.err.join("\n   ")}`;
		}
		if(this._outputBuffer.length>0) {
			text+=`\nstdout:\n   ${this.out.join("\n   ")}`;
		}
		log.debug(text);
	}

	/********************** Private Interface **********************/
	/**
	 * Processes input. If there are no filters then it goes straight in. IF there are then we let the filter determine how we categorize each line
	 * @param {"stdout"|"stderr"} type
	 * @param {Buffer} buffer
	 * @private
	 */
	_filter(type, buffer) {
		const push=(type, buffer)=> {
			if(type==="stdout") {
				this._outputBuffer+=buffer.toString();
			} else if(type==="stderr") {
				this._errorBuffer+=buffer.toString();
			} else {
				assert.toLog(type==null);
			}
		};
		if(this.filter) {
			// this is a little risky 'cause there is nothing to ensure that the buffers are complete.
			buffer.toString()
				.split(/\s*\n\s*/)
				.filter((line)=>line.length>0)
				.forEach((line)=> {
					push(this.filter(type, line), `${line}\n`);
				});
		} else {
			push(type, buffer);
		}
	}

	/**
	 * @param {string} buffer
	 * @returns {[string]}
	 * @private
	 */
	static _parse(buffer) {
		return buffer.split(/\s*\n+/).filter((line)=>line.length!==0);
	}
}

/**
 * Swagger puts it's stdout on stderr. We filter it so that it gets redirected
 * @typedef {CaptureStandardIO} CaptureSwaggerIO
 */
class CaptureSwaggerIO extends CaptureStandardIO {
	/**
	 * @param {{stdout:Readable, stderr:Readable}} emitter
	 * @param {Readable} stdout
	 * @param {Readable} stderr
	 */
	constructor({
		emitter=undefined,
		stdout=undefined,
		stderr=undefined
	}={}) {
		super({
			emitter,
			stdout,
			stderr,
			filter: (type, line)=>{
				if(type==="stderr") {
					if(line.match(/^\[\w+]\s*INFO\s+|DEBUG\s+/)) {
						return "stdout";
					}
				}
				return type;
			}
		});
	}
}


/**
 * This guy is meant to look like a standard out and error interface.  He is a pretty
 * dumb and simple beast that I think will primarily be used for testing
 */
class EmitStandardIO {
	constructor() {
		this.stderr=new PassThrough();
		this.stdout=new PassThrough();
	}

	close(code=0) {
		this.stderr.push(null); this.stderr.emit("close", code); this.stderr=null;
		this.stdout.push(null); this.stdout.emit("close", code); this.stdout=null;
	}

	writeOut(...args) {
		if(this.stdout) {
			args.forEach((arg)=>this.stdout.push(arg));
		}
	}

	writeError(...args) {
		if(this.stderr) {
			args.forEach((arg)=>this.stderr.push(arg));
		}
	}
}

exports.CaptureStandardIO=CaptureStandardIO;
exports.CaptureSwaggerIO=CaptureSwaggerIO;
exports.EmitStandardIO=EmitStandardIO;
