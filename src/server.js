/**
 * User: curtis
 * Date: 05/27/18
 * Time: 9:24 PM
 * Copyright @2018 by Xraymen Inc.
 */

const {Command}=require("pig-cmd").command;
const {ExpressServerCommand}=require("pig-cmd").express;
const {SeriesQueue}=require("pig-cmd").queue;
const {SetupDatabaseCommand}=require("./command/database/admin");
const {SetupEnvironmentCommand}=require("./command/setup");
const data_factory=require("./data/factory");
const route_factory=require("pig-cmd").route.factory;
const route_crud=require("./route/endpoint/crud");
const route_execute=require("./route/endpoint/execute");
const route_diagnostics=require("./route/endpoint/diagnostics");
const middle_auth=require("./route/middleware/auth");

/**
 * We want to make sure the application environment is loaded before we do anything with it which is why we isolate the
 * commands in the startup command - they are all run after the environment has been loaded
 */
class StartupCommand extends Command {
	execute(callback) {
		const application=data_factory.application.get(),
			queue=new SeriesQueue({verbose: true}),
			server=new ExpressServerCommand({
				name: application.name,
				nodenv: application.nodenv,
				port: application.server.port
			});
		configureServerRoutes(server.router);
		queue.addCommand(new SetupDatabaseCommand(application.getModuleInfo("mongo")));
		queue.addCommand(server);
		queue.execute(callback);
	}
}

/**
 * @param {Router} router
 */
function configureServerRoutes(router) {
	/****************** Middleware Routes ******************
	 * note: be careful about matching more than one route.
	 *******************************************************/
	/**
	 * Authentication middleware: Ensures that there is a user with valid credentials.  Will parse
	 * the details, look the user up and make the user accessible to subsequent handlers
	 */
	router.use(route_factory.create(middle_auth.Authenticate));

	/**** DIAGNOSTICS ****/
	router.get("/diagnostics/env", route_factory.create(route_diagnostics.Env));
	router.get("/diagnostics/ping", route_factory.create(route_diagnostics.Ping));

	/**** STORAGE ****/
	router.post("/api/project/create", route_factory.create(route_crud.CreateProject));
	router.delete("/api/project/delete/:id", route_factory.create(route_crud.DeleteProject));
	router.get("/api/project/get/:id", route_factory.create(route_crud.GetProject));
	router.put("/api/project/update", route_factory.create(route_crud.UpdateProject));
	router.get("/api/projects/get", route_factory.create(route_crud.GetProjects));

	/**** EXECUTION ****/
	router.put("/api/project/build/:id", route_factory.create(route_execute.BuildProject));
	router.put("/api/project/run/:id", route_factory.create(route_execute.RunProject));
	router.put("/api/project/status/:id", route_factory.create(route_execute.GetProjectStatus));
	router.put("/api/project/stop/:id", route_factory.create(route_execute.StopProject));
}

function startup() {
	const series=new SeriesQueue({verbose: true});
	series.addCommand(new SetupEnvironmentCommand());
	series.addCommand(new StartupCommand());
	series.execute(function(error) {
		if(error) {
			process.exit(1);
		}
	});
}

startup();
