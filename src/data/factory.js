/**
 * User: curtis
 * Date: 05/27/18
 * Time: 10:49 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const path=require("path");
const format=require("pig-core").format;
const file=require("pig-core").file;
const notification=require("pig-core").notification;
const {OpenApiV2}=require("./_openapi");
const data_model=require("./model");
const constant=require("../common/constant");


const storage={
	/**
	 * Manages our instances per application: application.local, application.dev, etc.
	 */
	application: {},
	model: {
		gamuts: null
	},
	nodenv: (process.env.NODE_ENV || constant.nodenv.LOCAL)
};


/**
 * Application factory.
 */
const application=exports.application={
	/**
	 * Gets a configuration for the specified application
	 * @param {Object} overrides
	 *  - nodenv {string}
	 *  - mode {string}
	 * @returns {Application}
	 */
	get(overrides=undefined) {
		let nodenv=storage.nodenv;
		if(_.has(overrides, "nodenv")) {
			nodenv=overrides.nodenv;
			overrides=_.omit(overrides, "nodenv");
		}
		if(storage.application[nodenv]) {
			// the application has already been configured nonetheless we will take overrides
			if(overrides) {
				_.merge(storage.application[nodenv].raw.application, overrides);
			}
		} else {
			exports.application.set(file.readToJSONSync(`./res/configuration/${nodenv}/settings.yaml`), overrides);
		}
		return storage.application[nodenv];
	},

	/**
	 * Sets a configuration for the specified application. The only time this should need to be called outside of the factory
	 * is when we are retrieving settings from our settings server
	 * @param {Object} settings
	 * @param {Object} overrides
	 */
	set(settings, overrides=undefined) {
		const nodenv=settings.env.name;
		settings=_.cloneDeep(settings);
		// resolve all of our module servers to wherever they should be depending on our current env
		_.forEach(settings.module, (module)=> {
			module.server=(process.env.HOST_ENV==="docker")
				? module.server.docker
				: module.server.debug;
		});
		// help keep the differences between apps easier to manage by setting up an "application" domain.
		// Once this is set then the differences in Application per module will be minimized
		settings.application=settings.module.server;
		_.merge(settings.application, overrides);
		storage.application[nodenv]=new data_model.Application(settings);
		this._applicationToEnvironment(storage.application[nodenv]);
		if(nodenv===storage.nodenv) {
			notification.emit(constant.event.SETTINGS_LOADED);
		}
	},

	/**
	 * Aligns the environment to our application
	 * @param {Application} application
	 * @private
	 */
	_applicationToEnvironment: function(application) {
		process.env.DEBUG=application.debug.enabled;
		process.env.VERBOSE=application.debug.verbose;
	}
};

/**
 * Factory objects having to do with models
 */
exports.model={
	/**
	 * Gets the model-gamuts.yaml object
	 * @returns {PigGamuts}
	 */
	get gamuts() {
		if(!storage.model.gamuts) {
			storage.model.gamuts=file.readToJSONSync("./res/configuration/model-gamuts.yaml");
		}
		return storage.model.gamuts;
	}
};

exports.openAPI={
	/**
	 * Factory function that builds a model (that we don't formally declare)
	 * @param {Project} project
	 * @param {string} env - the environment to use to resolve template variables in the project specification
	 * @returns {Object} - an OpenAPI compliant specification
	 */
	fromProject: (project, env=constant.project.env.PIG)=>new OpenApiV2(project, env).convert()
};

exports.project={
	/**
	 * Creates an instance of our Project model from an existing document
	 * @param {Object} data
	 * @returns {Project}
	 */
	create(data) {
		return new data_model.Project(data);
	},

	/**
	 * Creates a new instance from a spec
	 * @param {Object} spec
	 * @returns {Project}
	 */
	createFromSpec(spec) {
		return new data_model.Project({
			id: spec.id,
			build: {
				path: path.join(application.get().build.root, "deploy", spec.id, "nodejs"),
				status: exports.status.create()
			},
			stop: {
				status: exports.status.create()
			},
			run: {
				status: exports.status.create()
			},
			spec: spec
		});
	},

	/**
	 * Creates an instance of our Project model from an existing document
	 * @param {Object|Project} data
	 * @returns {Project}
	 */
	ensure(data) {
		return (data instanceof data_model.Project) ? data : new data_model.Project(data);
	}
};

exports.status={
	/**
	 * Creates an object with a schema that matches <code>Status</code>
	 * @param {string} value
	 * @param {Error|string|null} error
	 * @param {{out:[string], err:[string]}} stdio
	 * @param {Date} timestamp
	 * @returns {Status}
	 */
	create({
		value=constant.status.NONE,
		error=constant.status.NONE,
		stdio={
			out: [],
			err: []
		},
		timestamp=new Date()
	}={}) {
		return {
			error: format.messageToString(error),
			value,
			stdio,
			timestamp
		};
	}
};

/**
 * User factories
 */
exports.user={
	/**
	 * Gets user from our DB by id
	 * @param {string} id
	 * @returns {Promise}
	 */
	create: function(id) {
		return new Promise(function(resolve, reject) {
			resolve({
				id: "id",
				name: "test-user",
				email: "test-user@domain.com",
				permissions: new data_model.Permissions({
					read: true,
					write: true
				})
			});
		});
	}
};
