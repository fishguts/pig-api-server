/**
 * User: curtis
 * Date: 6/12/18
 * Time: 12:26 AM
 * Copyright @2018 by Xraymen Inc.
 *
 * This works with models as in the data model within a project. Not a model as in the model of this project.
 */

const _=require("lodash");
const _util=require("./_util");
const assert=require("pig-core").assert;
const {PigError}=require("pig-core").error;
const template=require("pig-core").template;
const constant=require("../common/constant");

/**
 * Translates our Pig models and views into an internal and intermediate graph <code>CompilerGraph</code>
 */
class ModelCompiler {
	/**************** Public Interface ****************/
	/**
	 * @param {OpenApiV2} openApi
	 */
	constructor(openApi) {
		this.openApi=openApi;
	}

	/**
	 * Takes an internal representation of an openAPI model and flattens it
	 * @param {CompilerGraph} graph
	 * @param {Number} maxDepth - how many property tiers to descend
	 * @returns {Array<CompilerNode|CompilerGraph>}
	 */
	flattenGraph(graph, maxDepth=Number.MAX_SAFE_INTEGER) {
		return this._modelToFlattened(graph, maxDepth);
	}

	/**
	 * Takes internal representations of openAPI models (such as response from and flattens them
	 * @param {Array<CompilerNode|CompilerGraph>} graphs
	 * @param {Number} maxDepth - maximum number of property tiers to descend
	 * @returns {Array<CompilerNode>}
	 */
	flattenGraphs(graphs, maxDepth=Number.MAX_SAFE_INTEGER) {
		return graphs.reduce((result, model)=>{
			return result.concat(this._modelToFlattened(model, maxDepth));
		}, []);
	}


	/**
	 * Converts a pig API model to our own internal representation
	 * @param {PigModel} model
	 * @returns {CompilerGraph}
	 * @private
	 */
	modelToGraph(model) {
		// all responses assume that model is the root.  Whether it is an array or object depends on the view's cardinality.
		const {root, leaf}=this._modelToNode({model});
		_.sortBy(model.fields, "name")
			.reduce((result, field)=> {
				return this._fieldToHierarchy({
					modelField: field,
					model,
					parent: result
				});
			}, leaf);
		return root;
	}

	/**
	 * Converts a request's fields into a model schema
	 * @param {PigModel} model
	 * @param {PigFieldOperation} requestField
	 * @returns {CompilerGraph}
	 */
	requestFieldToGraph({
		model,
		requestField
	}) {
		let modelField=_util.referenceFieldToField({model, reference: requestField});
		const mixins=Object.assign({
			variable: this._requestFieldToDefaultVariableName({modelField, requestField})
		}, _.pick(requestField, ["defaultValue", "encoding", "location", "operation", "propertyOf", "variable"]));

		// if this request field is an update operation then we want to filter out AUTO-IDs 'cause they should/will never be part of the request.
		const filter=(requestField.operation==="set")
			? (field)=>field.attributes.indexOf(constant.project.field.attribute.AUTO_ID)<0
			: undefined;

		if(!requestField._scalar && modelField.type===constant.project.field.type.OBJECT) {
			const {root, leaf}=this._modelToNode({model, mixins});
			this._fieldToHierarchy({filter, model, modelField, parent: leaf});
			return root;
		} else if(requestField._scalar || modelField.type===constant.project.field.type.ARRAY) {
			// A: if we have flagged the field as a <code>_scalar</code> then it means that we want to treat the field as an array
			// 	and the only time we do this is when the <code>function</code> is "create-many".
			// B: If <code>field._scalar</code> is not set then this is a little indeterminant and we have a ticket - [#4]. What has happened is the
			// 	object in the model is an array and they have specified it as a param. If the item they have designated as an array
			// 	has children and it is in the body then they should be okay. If not then we will make assumptions and treat it very simply.
			if(requestField.location===constant.project.request.param.location.BODY) {
				const {root, leaf}=this._modelToNode({model, mixins,
					subtype: requestField._scalar ? modelField.type : undefined,
					type: requestField._scalar ? constant.project.field.type.ARRAY : undefined
				});
				this._fieldToHierarchy({filter, model, modelField, parent: leaf});
				return root;
			} else {
				if(requestField._scalar) {
					modelField=Object.assign({}, modelField, {
						subtype: modelField.type,
						type: constant.project.field.type.ARRAY
					});
				}
				const {root, leaf}=this._fieldToNode(modelField, mixins);
				leaf.type=constant.project.field.type.STRING;
				return root;
			}
		} else {
			return this._fieldToNode(modelField, mixins).root;
		}
	}

	/**
	 * Translates the specified request-fields (built over the specified model) into a fully defined schema.
	 * @param {PigModel} model
	 * @param {Array<PigFieldOperation>} requestFields
	 * @returns {Array<CompilerGraph>}
	 */
	requestFieldsToGraphs(model, requestFields) {
		return requestFields.map(requestField=>(
			this.requestFieldToGraph({
				model,
				requestField
			})
		));
	}

	/**
	 * Translates the specified view's request (built over the specified model) into a fully defined schema.
	 * @param {PigModel} model
	 * @param {PigView} view
	 * @returns {Array<CompilerGraph>}
	 */
	requestToGraphs(model, view) {
		let requestFields=view.requestFields;
		if(view.function===constant.project.request.function.CREATE_MANY) {
			// the scalar nature of the request fields designated for updating the model are implied by the <code>function</code>
			// Here we make the rubber meet the road and convert him to a scalar.
			requestFields=requestFields.map(field=>{
				return (field.operation===constant.project.request.param.operation.UPDATE)
					? Object.assign({}, field, {_scalar: true})
					: field;
			});
		}
		return this.requestFieldsToGraphs(model, requestFields);
	}

	/**
	 * Converts a response field into a model schema
	 * @param {PigModel} model
	 * @param {PigView} view
	 * @param {PigFieldReference} responseField
	 * @returns {CompilerGraph}
	 * @private
	 */
	responseFieldToGraph({
		model,
		view,
		responseField
	}) {
		const modelField=_util.referenceFieldToField({model, reference: responseField}),
			responseType=_.includes([
				constant.project.request.function.CREATE_MANY,
				constant.project.request.function.GET_MANY,
				constant.project.request.function.UPDATE_MANY
			], view.function)
				? constant.project.field.type.ARRAY
				: constant.project.field.type.OBJECT,
			// all responses assume that model is the root.  Whether it is an array or object depends on the view's cardinality.
			{root, leaf}=this._modelToNode({
				model,
				subtype: (responseType===constant.project.field.type.ARRAY)
					? modelField.type
					: undefined,
				type: responseType
			});
		this._fieldToHierarchy({model, modelField, parent: leaf});
		return root;
	}

	/**
	 * Translates the specified view's response (built over the specified model) into a fully defined schema.
	 * @param {PigModel} model
	 * @param {PigView} view
	 * @returns {CompilerGraph}
	 */
	responseToGraph(model, view) {
		// why do we sort? We want to process these guys from top of the tree to bottom. This will ensure
		// that any wildcard references appear up front and build the base for our schema.
		return _.chain(view.responseFields)
			.sortBy("name")
			.reduce((result, responseField)=>{
				return _.merge(result, this.responseFieldToGraph({
					model,
					view,
					responseField
				}));
			}, {})
			.value();
	}

	/**************** Private Interface ****************/
	/**
	 * Creates an object out of this guy according to his type. He does no path descending.
	 * @param {PigField} field
	 * @param {Object} mixins
	 * @returns {{root: CompilerNode, leaf: (Object|undefined)}}
	 * @private
	 */
	_fieldToNode(field, mixins=null) {
		const path=field.name.replace(/\.?\*$/, ""),
			split=path.split("."),
			/**
			 * Here we selectively chose from allowed depending on the effective data type
			 * @param {string} type
			 */
			_mixinAllowed=(type)=>{
				if(type===constant.project.field.type.ENUM) {
					result.enum=_.get(field.allowed, "values");
				} else if(type===constant.project.field.type.STRING) {
					result.pattern=_.get(field.allowed, "regex");
				}
			};

		// 1. first let's extract the essentials and clean him up
		const result=Object.assign({
			name: _.last(split),
			path
		}, _.omit(field, ["allowed", "name", "subtype"]), mixins);
		if(_.isString(result.defaultValue)) {
			result.defaultValue=template.render(result.defaultValue, this.openApi.variables);
		}
		// 2. now apply type specific treatement
		if(field.type===constant.project.field.type.OBJECT) {
			result.properties={};
			return {root: result, leaf: result.properties};
		} else if(field.type===constant.project.field.type.ARRAY) {
			const subtype=field.subtype || constant.project.field.type.OBJECT;
			result.items={
				type: subtype
			};
			_mixinAllowed(subtype);
			if(subtype===constant.project.field.type.OBJECT) {
				result.items.properties={};
				return {root: result, leaf: result.items.properties};
			} else if(subtype===constant.project.field.type.ARRAY) {
				// without a subtype we have another problem here.
				throw new PigError({
					message: "_model._fieldToNode called with array of arrays. We don't have proper support for it"
				});
			} else {
				return {root: result, leaf: result.items};
			}
		} else {
			_mixinAllowed(field.type);
		}
		return {root: result};
	}

	/**
	 * Always assumes that this field lives in an object (in a property). He either finds the path or creates the path
	 * for this field and returns the object.  He also expands "objects" into the properties that defines them and this
	 * is why we ask for the model.
	 * Note: the addition of the model was a refactor because we always want to do object expansion. But we could take advantage
	 *  of having it and be smarter about finding unknown properties in the middle. Will add a to-do below.
	 * @param {function(field:PigField):boolean} filter - return true to process field and false to bypass
	 * @param {PigField} modelField
	 * @param {PigModel} model
	 * @param {Object} parent
	 * @returns {CompilerGraph} - same as parent when function is entered
	 * @private
	 */
	_fieldToHierarchy({modelField, model,
		filter=null,
		parent={}
	}) {
		/**
		 * @param {PigField} modelField
		 */
		const _processField=(modelField)=>{
			if(!filter || filter(modelField)) {
				const path=modelField.name
					.replace(/\.\*$/, "")		// strip off all terminating ".*"
					.split(".");
				let lastLeaf=parent;
				// iterate through and on the way create this guys property path
				for(let index=0; index<path.length-1; index++) {
					try {
						if(!lastLeaf[path[index]]) {
							// The tier does not exist yet. Let's use the model and see if we can "intelligently" create it.
							let _field=_.find(model.fields, {
								name: path.slice(0, index+1).join(".")
							});
							if(_field) {
								_processField(_field);
							}
						}
						if(_.has(lastLeaf[path[index]], "properties")) {
							assert.properties(lastLeaf[path[index]], "properties");
							lastLeaf=lastLeaf[path[index]].properties;
						} else if(_.has(lastLeaf[path[index]], "items")) {
							lastLeaf=lastLeaf[path[index]].items;
							// he is in the middle so he must be an array of or an array of arrays. We will find the fella.
							if(lastLeaf.type===constant.project.field.type.OBJECT) {
								lastLeaf=lastLeaf.properties;
							} else {
								assert.toLog(lastLeaf.type===constant.project.field.type.ARRAY);
								lastLeaf=lastLeaf.items;
							}
						} else {
							// We did not find him which means that the user spanned a gap. We are assuming "object" unspecified middle tier properties.
							lastLeaf[path[index]]={
								name: path[index],
								path: path.slice(0, index+1).join("."),
								type: constant.project.field.type.OBJECT,
								properties: {}
							};
							lastLeaf=lastLeaf[path[index]].properties;
						}
					} catch(error) {
						throw new PigError({
							error: error,
							message: `Experienced a problem with field ${modelField.name}`
						});
					}
				}
				lastLeaf[_.last(path)]=this._fieldToNode(modelField).root;
			}
		};

		if(modelField.type===constant.project.field.type.OBJECT) {
			const fields=(modelField.name==="*")
				// this is our own non-existent field which represents the whole model.
				? model.fields.filter((_field)=>_field.id!==modelField.id)
				// find all fields in our spec that have a prefix matching our input field's path
				: model.fields.filter((_field)=>_field.name.startsWith(modelField.name.replace("*", "")));
			// we want to make sure that we create our hierarchy from the bottom up so that we properly create all of the objects
			// in the middle and get their types right. So we sort which should always put dependent objects higher up in the list.
			_.sortBy(fields, "name").forEach(_processField);
		} else {
			_processField(modelField);
		}
		return parent;
	}

	/**
	 * Creates a node out of this model. Entirely meant for roots in which the model itself is implied
	 * @param {PigModel} model
	 * @param {Object} mixins
	 * @param {string} subtype
	 * @param {string} type
	 * @returns {{root: CompilerNode, leaf: (Object|undefined)}}
	 * @private
	 */
	_modelToNode({model, mixins=null, subtype=undefined, type=constant.project.field.type.OBJECT}) {
		return this._fieldToNode({
			desc: model.desc || `${model.name} model object`,
			name: model.name,
			subtype,
			type
		}, mixins);
	}

	/**
	 * Converts a model cooked up by one of (request|response)ToModel functions and flattens him into a set of fields.
	 * @param {PigModel} model
	 * @param {Number} maxDepth - how many property tiers to descend
	 * @returns {Array<CompilerNode>}
	 * @private
	 */
	_modelToFlattened(model, maxDepth=Number.MAX_SAFE_INTEGER) {
		const _processNode=(node, result=[], depth=0)=> {
			if(depth>=maxDepth) {
				result.push(node);
			} else {
				if(node.type===constant.project.field.type.OBJECT) {
					// if he is not a terminal then we want his goodies within.
					if(_.isEmpty(node.properties)) {
						result.push(node);
					} else {
						Object.keys(node.properties).forEach((key)=>_processNode(node.properties[key], result, depth+1));
					}
				} else if((node.type===constant.project.field.type.ARRAY) && (node.items.type===constant.project.field.type.OBJECT)) {
					Object.keys(node.items.properties).forEach((key)=>_processNode(node.items.properties[key], result, depth+1));
				} else {
					result.push(node);
				}
			}
			return result;
		};
		return _.isEmpty(model) ? [] : _processNode(model);
	}

	/**
	 * Matches the logic we apply in the client to create normal variable names (if applicable)
	 * @param {PigField} modelField
	 * @param {PigFieldOperation} requestField
	 * @returns {string|undefined}
	 * @private
	 */
	_requestFieldToDefaultVariableName({modelField, requestField}) {
		if(!_.includes([
			constant.project.request.param.location.BODY,
			constant.project.request.param.location.LITERAL
		], requestField.location)) {
			return _.chain(modelField.name)
				.split(".")
				.last()
				.value();
		}
		return undefined;
	}
}

exports.ModelCompiler=ModelCompiler;
