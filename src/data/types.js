/**
 * User: curtis
 * Date: 9/7/18
 * Time: 9:19 PM
 * Copyright @2018 by Xraymen Inc.
 *
 * A collection of JSDoc definitions of various types that we use that do not define themselves
 * Note: everything prefaced with "pig" has to do with our project specification and open-api extensions
 */

/********************* Simple Types *********************/
/**
 * A type that is used predominantly where we want be able to plop lists in list/selection controls. But its use
 * extends beyond client code as we use this format in our <link>res/configuration/model-gamuts.yaml</link>
 * @typedef {Array<{text:string, value:string}>} ControlItemList
 */

/**
 * Defaults for the various types we support may be encoded. This type is a list of all supported encodings
 * @typedef {"none"|"string"|"space-delimited"|"comma-delimited"|"semi-delimited"|"punctuation-delimited"} PigDefaultEncoding
 */

/**
 * All known pig types that a field type may be. Definitive source is <link>res/configuration/model-gamuts.yaml</link>
 * @typedef {"array"|"boolean"|"date"|"enum"|"id"|"integer"|"object"|"number"|"string"} PigFieldType
 */

/**
 * All functions that may be assigned to a view. Definitive source is <link>res/configuration/model-gamuts.yaml</link>
 * @typedef {"count"|"create-one"|"create-many"|"delete-one"|"delete-many"|"distinct"|"get-one"|"get-many"|"update-one"} PigFunction
 */

/**
 * All operations that may be assigned to a param. Definitive source is <link>res/configuration/model-gamuts.yaml</link>
 * @typedef {"set"|"eq"|"ne"|"lt"|"lte"|"gt"|"gte"} PigOperation
 */

/********************* type definitions having to do with the Pig specification *********************/
/**
 * A single node in a graph.
 * @typedef {Object} CompilerNode
 * @property {Array<string>} attributes
 * @property {*|undefined} defaultValue
 * @property {number|undefined} depth	- only included when flattened
 * @property {string} desc
 * @property {Array<string>|undefined} enum
 * @property {string} id - id of the field in the model
 * @property {undefined|{allowed:Array<string>, properties:Object<string,CompilerNode>, subtype:string}} items
 * @property {string} name
 * @property {string} path
 * @property {string|undefined} pattern
 * @property {Object<string,CompilerNode>|undefined} properties
 * @property {PigFieldType|undefined} subtype
 * @property {PigFieldType} type
 * @property {Array<PigView>} views
 */

/**
 * A fully defined graph which due to recursive definition is just a node
 * @typedef {CompilerNode} CompilerGraph
 */

/**
 * Description of our <link>res/configuration/model-gamuts.yaml</link.
 * @typedef {Object} PigGamuts
 * @property {Object} database
 * @property {ControlItemList} database.types
 * @property {Object} log
 * @property {ControlItemList} log.level
 * @property {Object} model
 * @property {Object} model.field
 * @property {ControlItemList} model.field.attributes
 * @property {ControlItemList} model.field.types
 * @property {Array<PigField>} model.fields
 * @property {Array<PigFieldReference|PigFieldOperation>} model.fieldReferences
 * @property {Array<ControlItemList>} model.operators
 * @property {Object<string, {name:string}>} model.transform
 * @property {Object} model.view
 * @property {ControlItemList} model.view.functions
 * @property {Object} request
 * @property {Object} request.param
 * @property {ControlItemList} request.param.encoding
 * @property {ControlItemList} request.param.location
 * @property {ControlItemList} targets
 * @property {Object} transport
 * @property {ControlItemList} transport.types
 * @property {Object} variable
 * @property {ControlItemList} variable.scope
 */

/**
 * @typedef {Object} PigInfo
 * @property {string} contact
 * @property {string} desc
 * @property {{terms:{name:string, url:string}, license:{name:string, url:string}}} legal
 * @property {string} name
 * @property {string} version
 *
 */

/**
 * @typedef {Object} PigModel
 * @property {Object<string, *>} attributes
 * @property {string} desc
 * @property {string} id
 * @property {Array<PigField>} fields
 * @property {string} name
 * @property {Array<PigView>} views
 */

/**
 * @typedef {Object} PigView
 * @property {string} desc
 * @property {PigFunction} function
 * @property {string} id
 * @property {string} name
 * @property {string} path
 * @property {Array<PigFieldOperation>} requestFields
 * @property {Array<PigFieldReference>} responseFields
 * @property {string} serviceId
 * @property {string|null} timeout
 * @property {{method:string, protocol:"http"|"https"}} transport
 */

/**
 * @typedef {Object} PigField
 * @property {Object} allowed
 * @property {string|undefined} allowed.regex
 * @property {Array<string>|undefined} allowed.values
 * @property {Array<string>} attributes
 * @property {string} desc
 * @property {string} id
 * @property {string} name
 * @property {PigFieldType|undefined} subtype
 * @property {PigFieldType} type
 * @property {Array<PigView>} views
 */

/**
 * @typedef {Object} PigFieldReference
 * @property {*} defaultValue
 * @property {string} fieldId
 * @property {string} id
 * @property {"model"|"query"|"result"} propertyOf
 */

/**
 * @typedef {PigFieldReference} PigFieldOperation
 * @property {boolean|undefined} _scalar - an internally managed property that we set if a request field's cardinality should be >1
 * @property {string} encoding
 * @property {"body"|"header"|"path"|"query"} location
 * @property {PigOperation} operation
 * @property {string} variable
 */

/**
 * @typedef {Object} PigSpecification
 * @property {Object} build
 * @property {string} id
 * @property {PigInfo} info
 * @property {Array<Object>} databases
 * @property {Array<Object>} envs
 * @property {Array<PigModel>} models
 * @property {PigUser} owner
 * @property {Array} proxies
 * @property {Object} server
 * @property {Object} variables
 * @property {Array<string>} targets
 * @property {Date} timestampCreated
 * @property {Date} timestampUpdated
 */

/**
 * @typedef {Object} PigUser
 * @property {string} id
 * @property {string} name
 * @property {Object} authorization
 */

