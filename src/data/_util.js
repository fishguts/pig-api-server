/**
 * User: curtis
 * Date: 9/15/18
 * Time: 7:08 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const {PigError}=require("pig-core").error;
const model_factory=require("./factory");
const constant=require("../common/constant");

/**
 * Looks up the field in among our canned fields returns it. Throws PigError if it cannot be found.
 * @param {string} id
 * @param {PigGamuts} gamuts
 * @returns {PigField}
 */
exports.fieldIdToGamutsField=function({id,
	gamuts=model_factory.model.gamuts
}) {
	const field=_.find(gamuts.model.fields, {id});
	if(field) {
		return field;
	} else {
		throw new PigError({
			details: `field "${id}" not found among predefined fields`,
			message: "Unable to resolve internal field reference"
		});
	}
};

/**
 * Looks up the field in the model and returns it. Throws PigError if it cannot be found.
 * @param {string} id
 * @param {PigModel} model
 * @returns {PigField}
 */
exports.fieldIdToModelField=function({id, model}) {
	const field=_.find(model.fields, {id});
	if(field) {
		return field;
	} else {
		throw new PigError({
			details: `field ${id} not found in model ${model.name}`,
			message: "Unable to resolve model field reference"
		});
	}
};

/**
 * Figures out what type of reference it is and then finds him
 * @param {PigModel} model
 * @param {PigFieldReference} reference
 * @returns {PigField}
 */
exports.referenceFieldToField=function({model, reference}) {
	if(reference.propertyOf===constant.project.field.propertyOf.MODEL) {
		return exports.fieldIdToModelField({id: reference.fieldId, model});
	} else {
		const field=Object.assign({}, exports.fieldIdToGamutsField({id: reference.fieldId}));
		// we inherit our gamuts from the editor project. Here we make some alterations to him
		// for his more specific role here versus his more general purpose in the editor
		// 1. strip off the query field decorations
		field.name=_.get(field.name.match(/^\[([^\]]+)]$/), 1, field.name);
		// 2. update his attributes
		if(reference.defaultValue===undefined) {
			if(field.attributes===undefined) {
				field.attributes=[constant.project.field.attribute.REQUIRED];
			} else if(field.attributes.indexOf(constant.project.field.attribute.REQUIRED)<0) {
				field.attributes.push(constant.project.field.attribute.REQUIRED);
			}
		}
		return field;
	}
};

