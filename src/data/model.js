/**
 * User: curtis
 * Date: 05/27/18
 * Time: 10:49 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const path=require("path");
const http=require("pig-core").http;
const _variable=require("./_variable");
const constant=require("../common/constant");


/**
 * Base class. All work with blob of data.
 */
class Model {
	/**
	 * @param {Object} data
	 */
	constructor(data) {
		this._data=data;
	}

	/**
	 * Gets the raw object that is this object
	 * @returns {Object}
	 */
	get raw() {
		return this._data;
	}

	clone(deep=true) {
		const result=new this.constructor();
		result._data=(deep)
			? _.cloneDeep(this._data)
			: _.clone(this._data);
		return result;
	}
}

/**
 * Application information. Largely a server for our application configuration info with an application jobQueue.
 * @typedef {Model} Application
 */
class Application extends Model {
	constructor(data) {
		super(data);
	}

	/**** Application Environment Information ****/
	/**
	 * NODE_ENV or our own default
	 * @returns {string}
	 */
	get nodenv() { return this._data.env.name; }
	get isLowerEnv() { return !this.isUpperEnv; }
	get isUpperEnv() { return this.nodenv===constant.nodenv.STAGING || this.nodenv===constant.nodenv.PRODUCTION; }

	/**
	 * Debug information
	 * @returns {{enabled:Boolean, verbose:Boolean}}
	 */
	get debug() { return this._data.application.debug; }

	/**
	 * Log configuration information
	 * @returns {{level:string, transports:[{lean:Boolean, overrides:Object, type:string}]}}
	 */
	get log() { return this._data.application.log; }

	/**
	 * Integration information about how we should behave in this environment
	 * @returns {{bypass: [string]}}
	 */
	get integration() { return this._data.application.integration; }

	/**** Application Specific Details ****/
	/**
	 * Child process build info
	 * @returns {{command:string, root:string, params:[string]}}
	 */
	get build() { return this._data.application.build; }

	/**
	 * Mode in which this guy is running. See <code>constant.application.mode</code>
	 * @returns {string}
	 */
	get mode() { return this._data.application.mode; }

	/**
	 * Name of the project
	 * @returns {string}
	 */
	get name() { return this._data.application.name; }

	/**
	 * Application's own server details
	 * @returns {{host:string, port:Number}}
	 */
	get server() { return this.getModuleServer(this._data.application.name); }

	/**
	 * Builds uri for current host. Entirely for logging
	 * @param {string} path
	 * @returns {Url}
	 */
	getServerUrl(path=undefined) { return this.getModuleServerUrl(this._data.application.name, path); }

	/**
	 * Gets metadata information about our servers. See constant.module for module names
	 * @param {string} name
	 * @returns {{name:string, server:Object<string, {host:string, port:Number}>}}
	 */
	getModuleInfo(name) { return this._data.module[name]; }

	/**
	 * Gets the server host and port. It determines whether to use the container properties or other depending on whether this
	 * client is in a docker network or not. We track this via the environment variable (see Dockerfile) HOST_ENV
	 * @param {string} name
	 * @returns {{host:string, port:Number}}
	 */
	getModuleServer(name) {
		return this._data.module[name].server;
	}

	/**
	 * Builds a Url to the target module
	 * @param {string} name
	 * @param {string} path
	 * @returns {Url}
	 */
	getModuleServerUrl(name, path=undefined) {
		return new http.Url(this.getModuleServer(name), path);
	}

	/**
	 * Do we want to bypass interaction with this module (where possible). Immediately is concerned with settings.
	 * @param {string} name
	 * @returns {boolean}
	 */
	isBypassModuleIntegration(name) { return _.includes(_.get(this._data.application.integration, "bypass"), name); }
}


/**
 * Describes a project's spec and has information about it's build and execution
 * @typedef {Model} Project
 */
class Project extends Model {
	/**
	 * @returns {string}
	 */
	get id() { return this.spec.id; }

	/**
	 * @returns {string}
	 */
	get name() {
		return _variable.renderTemplateString({
			string: this.spec.info.name,
			variables: this.spec.variables
		});
	}

	/**
	 * Gets an absolute build path for this project
	 * @returns {string}
	 */
	get absBuildPath() { return path.resolve(this.build.path); }

	/**
	 * Information about where the build is and in what state it is in.
	 * Note: the path points to a single target which we designate as the "nodejs" build. This is not a
	 * restriction on the project but rather our localized, pig build environment.
	 * @returns {{path:string, status:Status}}
	 */
	get build() { return this._data.build; }
	set build(value) { this._data.build=value; }

	/**
	 * Execution information
	 * @returns {{path:string, pid:Number, status:Status}}
	 */
	get run() { return this._data.run; }
	set run(value) { this._data.run=value; }

	/**
	 * Kill information
	 * @returns {{status:Status}}
	 */
	get stop() { return this._data.stop; }
	set stop(value) { this._data.stop=value; }
	/**
	 * Full blown specification (our own). There is a formal model declared in the editor.
	 * @returns {PigSpecification}
	 */
	get spec() { return this._data.spec; }
}

/**
 * Permissions a user is granted
 * @typedef {Model} Permissions
 */
class Permissions extends Model {
	canRead() { return this._data.read; }
	canWrite() { return this._data.write; }
}

/**
 * Describes a single state of status. He is not a top level model object.  He is more of a type.
 */
class Status {
	/**
	 * See constant.status
	 * @returns {string}
	 */
	get value() {return this._data.value;}
	set value(value) {this._data.value=value;}

	/**
	 * If value is in a failure state then this guy tells you why
	 * @returns {string}
	 */
	get error() {return this._data.error;}
	set error(value) {this._data.error=value;}

	/**
	 * @returns {{out:[string], error:[string]}}
	 */
	get stdio() { return this._data.stdio; }
	/**
	 * @returns {Date}
	 */
	get timestamp() { return this._data.timestamp; }
}

/**
 * Represents a user and some of his profile info
 * @typedef {Model} User
 */
class User extends Model {
	/**
	 * Unique urn of this user
	 * @returns {string}
	 */
	get id() { return this._data.id; }
	/**
	 * @returns {string}
	 */
	get name() { return this._data.name; }
	/**
	 * @returns {string}
	 */
	get email() { return this._data.email; }
	/**
	 * @returns {Permissions}
	 */
	get permissions() { return this._data.permissions; }
}


exports.Application=Application;
exports.Project=Project;
exports.Permissions=Permissions;
exports.Status=Status;
exports.User=User;
