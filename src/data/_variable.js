/**
 * User: curtis
 * Date: 3/7/18
 * Time: 4:22 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const assert=require("pig-core").assert;
const template=require("pig-core").template;

/**
 * A well defined type for interacting with <code>Variables</code>
 */
class Variable {
	/**
	 * @param {string} id
	 * @param {string} name
	 * @param {string} scope
	 * @param {*} value
	 * @param {"number"|"string"} type
	 * @param {string} env - id
	 */
	constructor({id, name, scope, type, value, env=null}) {
		assert.ok(id!=null);
		this.env=env;
		this.id=id;
		this.name=name;
		this.scope=scope;
		this.type=type;
		this.value=value;
	}
}


/**
 * Gets the current state of the specified variable for the specified environment by ID
 * @param {Object} variables - var object as per our spec
 * @param {string} id - variable id
 * @param {Object} locals - optional, locally defined variables. Expected to be {key:value}
 * @param {string} env - id or object
 * @param {string} scope
 * @returns {Variable}
 */
exports.get=function({variables, id, env=null, locals=null, scope="local"}) {
	if(_.has(locals, id)) {
		new Variable({
			env: env,
			id: id,
			name: id,
			scope: scope,
			type: typeof(locals[id]),
			value: locals[id]
		});
	} else {
		const path=_getPath(id, env),
			varNode=_.get(variables, path);
		return (varNode)
			? new Variable({
				env: env,
				id: id,
				name: variables[id].default.name,
				scope: variables[id].default.scope,
				type: variables[id].default.type,
				value: varNode.value
			})
			: null;
	}
};

/**
 * Gets the current state of the specified variable for the specified environment by ID
 * @param {Object} variables - var object as per our spec
 * @param {string} env - id or object
 * @returns {[Variable]}
 */
exports.getAll=function(variables, env=null) {
	return Object.keys(variables||{})
		.reduce((result, id)=> {
			const variable=this.get({variables, id, env}) || ((env) ? this.get({variables, id}) : null);
			if(variable) {
				result.push(variable);
			}
			return result;
		}, []);
};


/**
 * It's just a convinience function that draws together <code>template</code> and <code>toMap</code>
 * @param {string} string - template string
 * @param {Object} variables
 * @param {string} env
 * @param {Object} mixins - optional key/values that you want to add to the mix
 * @returns {string}
 */
exports.renderTemplateString=function({string, variables, env=null, mixins=null}) {
	return template.render(string, exports.toMap({variables, env, mixins}));
};

/**
 * He will capture the state of the environments and create a key/value map
 * @param {Object} variables
 * @param {string} env
 * @param {Object} mixins - optional key/values that you want to add to the mix
 * @returns {string}
 */
exports.toMap=function({variables, env=null, mixins=null}) {
	return Object.assign(
		exports.getAll(variables, env).reduce((result, variable)=>{
			result[variable.name]=variable.value;
			return result;
		}, {}),
		mixins
	);
};


/**************** Private Interface ****************/
/**
 * Gets the property path for the variable
 * @param {string} id
 * @param {string} env
 * @returns {string}
 * @private
 */
function _getPath(id, env=null) {
	return (env)
		? `${id}.${_.isObject(env) ? env.id : env}`
		: `${id}.default`;
}

