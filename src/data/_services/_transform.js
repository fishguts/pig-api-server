/**
 * User: curtis
 * Date: 7/15/18
 * Time: 12:07 AM
 * Copyright @2018 by Xraymen Inc.
 */

const constant=require("../../common/constant");

exports.TransformServiceFactory=class {
	/**
	 * Creates a mongo service definition
	 * @param {OpenApiV2} openApi
	 * @param {Object} configuration
	 */
	constructor(openApi, configuration) {
		this.openApi=openApi;
		this.configuration=configuration;
	}

	/**
	 * Creates an openAPI x-pig-extension configuration
	 * @returns {Object}
	 */
	createserviceconfiguration() {
		// todo: this is just a placeholder
		return {
			id: this.configuration.id,
			class: constant.project.service.transform.CLASS,
			description: this.configuration.desc,
			name: this.configuration.name,
			type: this.configuration.type
		};
	}

	/**
	 * Translates a view into x-pig.operations
	 * @param {Object} model
	 * @param {Object} view
	 * @returns {[Object]}
	 */
	buildOperations(model, view) {
		this.openApi._addWarning({
			detail: "TransformServiceFactory.buildOperations() not implemented",
			severity: constant.severity.ERROR,
			subject: "service"
		});
		return [];
	}

	/**
	 * Translates a view parameter into it's x-pig extensions
	 * @param {Object} view
	 * @param {Object} parameter
	 * @returns {Object}
	 */
	buildParameterExtension(view, parameter) {
		this.openApi._addWarning({
			detail: "TransformServiceFactory.buildParameterExtension() not implemented",
			severity: constant.severity.ERROR,
			subject: "service"
		});
		return [];
	}
};
