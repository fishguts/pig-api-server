/**
 * User: curtis
 * Date: 7/1/18
 * Time: 9:02 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const {HTTPServiceFactory}=require("./_http");
const {DatabaseServiceFactory}=require("./_database");
const {TransformServiceFactory}=require("./_transform");
const constant=require("../../common/constant");

exports.ServiceFactory=class {
	/**
	 * @param {OpenApiV2} openApi
	 */
	constructor(openApi) {
		this.openApi=openApi;
	}

	/**************** Public Interface ****************/
	/**
	 * Builds definitions for all services found in the instance of OpenAPI
	 * @returns {[Object]}
	 */
	buildServiceConfigurations() {
		return _.map(this.openApi.spec.databases, this._buildDatabaseFactory.bind(this))
			.concat(_.map(this.openApi.spec.proxies, this._buildProxyFactory.bind(this)))
			.concat(_.map(this.openApi.spec.transforms, this._buildTransformFactory(this)))
			.reduce((result, factory)=>{
				try {
					if(factory) {
						result.push(factory.createServiceConfiguration());
					}
				} catch(error) {
					this.openApi._addWarning({
						detail: `failed to create service configuration of type="${factory.configuration.type}": ${error.message}`,
						severity: constant.severity.ERROR,
						subject: "service"
					});
				}
				return result;
			}, []);
	}

	/**
	 * Translates a view into x-pig.operations
	 * @param {PigModel} model
	 * @param {PigView} view
	 * @returns {[Object]}
	 */
	buildOperations(model, view) {
		const service=this._idToFactory(view.serviceId);
		return (service)
			? service.buildOperations(model, view)
			: [];
	}

	/**
	 * Translates a view parameter into it's x-pig extensions
	 * @param {PigView} view
	 * @param {CompilerNode} parameter
	 * @returns {Object}
	 */
	buildParameterExtension(view, parameter) {
		if(!view.serviceId) {
			return undefined;
		}
		const service=this._idToFactory(view.serviceId);
		return (service)
			? service.buildParameterExtension(view, parameter)
			: undefined;
	}


	/**************** Private Interface ****************/
	/**
	 * @param {Object} database
	 * @returns {DatabaseServiceFactory}
	 * @private
	 */
	_buildDatabaseFactory(database) {
		return new DatabaseServiceFactory(this.openApi, database);
	}

	/**
	 * @param {Object} proxy
	 * @returns {HTTPServiceFactory}
	 * @private
	 */
	_buildProxyFactory(proxy) {
		if(proxy.type===constant.project.service.proxy.type.http) {
			return new HTTPServiceFactory(this.openApi, proxy);
		} else {
			this.openApi._addWarning({
				detail: `unknown proxy "${proxy.type}"`,
				severity: constant.severity.ERROR,
				subject: "service"
			});
		}
	}

	_buildTransformFactory(transform) {
		return new TransformServiceFactory(this.openApi, transform);
	}

	/**
	 * Looks among the various places in which services can hide and finds the one associated with <code>id</code>
	 * @param {string} id
	 * @returns {Object|undefined}
	 * @private
	 */
	_idToFactory(id) {
		let data;
		if(id) {
			if(data=_.find(this.openApi.spec.databases, {id})) {
				return this._buildDatabaseFactory(data);
			} else if(data=_.find(this.openApi.spec.proxies, {id})) {
				return this._buildProxyFactory(data);
			} else if(data=_.find(this.openApi.spec.transforms, {id})) {
				return this._buildTransformFactory(data);
			} else {
				this.openApi._addWarning({
					detail: `unknown serviceId "${id}"`,
					severity: constant.severity.ERROR,
					subject: "service"
				});
			}
		}
	}
};
