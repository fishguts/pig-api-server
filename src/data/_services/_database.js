/**
 * User: curtis
 * Date: 6/24/18
 * Time: 1:39 AM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const assert=require("pig-core").assert;
const template=require("pig-core").template;
const util=require("pig-core").util;
const {ModelCompiler}=require("../_model");
const constant=require("../../common/constant");

/**
 * This was initially created to handle mongo but hopefully it can remain general enough to serve as a
 * general purpose database service layer. At least within the client and server. Of course the actual
 * application itself will probably not be so lucky.
 */
class DatabaseServiceFactory {
	/**
	 * Creates a database service definition
	 * @param {OpenApiV2} openApi
	 * @param {Object} configuration
	 */
	constructor(openApi, configuration) {
		this.openApi=openApi;
		this.configuration=configuration;
	}

	/**
	 * Creates an openAPI x-pig-extension configuration
	 * @returns {{id: string, connection:string, desc:string, name:string, type:string}}
	 */
	createServiceConfiguration() {
		return {
			id: this.configuration.id,
			class: constant.project.service.database.CLASS,
			// we are intentionally pointing the service name to the desc. We use the database name for the schema's name.
			name: this.configuration.desc,
			type: this.configuration.type,
			collections: this._createCollections(),
			database: {
				attributes: this.configuration.attributes,
				connectionString: template.render(this.configuration.connection, this.openApi.variables),
				name: template.render(this.configuration.name, this.openApi.variables)
			}
		};
	}

	/**
	 * Translates a view into x-pig.operations
	 * @param {PigModel} model
	 * @param {PigView} view
	 * @returns {Array<Object>}
	 */
	buildOperations(model, view) {
		return [{
			serviceId: this._modelIdToServiceId(model.id),
			type: view.function
		}];
	}

	/**
	 * Translates a view parameter into it's x-pig extensions
	 * @param {PigView} view
	 * @param {CompilerNode} parameter
	 * @returns {Array<Object>}
	 */
	buildParameterExtension(view, parameter) {
		const result={
			attributes: parameter.attributes,
			dataPath: parameter.path,
			id: parameter.id,
			operation: parameter.operation
		};
		if(parameter.propertyOf===constant.project.field.propertyOf.QUERY) {
			result.function=constant.project.request.param.function.QUERY;
		} else {
			// we include a flattened map of fields implied by this update or filter so that middleware may
			// be able to completely map this param to all DB fields.
			result.flattened=this.openApi.modelCompiler.flattenGraph(parameter)
				.map(node=>({
					depth: node.depth,
					id: node.id,
					path: node.path
				}));
			if(parameter.operation===constant.project.request.param.operation.UPDATE) {
				result.function=constant.project.request.param.function.UPDATE;
			} else {
				result.function=constant.project.request.param.function.FILTER;
			}
		}
		return result;
	}

	/**************** Private Interface ****************/
	/**
	 * Breaks the fields list up into the different types of fields that can exist in a list:
	 *  - filter: a field/param designated to be used as a filter to refine the response.
	 *  - query: a field/param that alters attributes of the query itself. For example, limit, skip, sort, etc.
	 *  - update: a field/param that will carry data intended to update the model.
	 * @param {Array<PigFieldOperation>} fields
	 * @returns {{filter: Array<PigFieldOperation>, query: Array<PigFieldOperation>, update: Array<PigFieldOperation>}}
	 */
	static _categorizeRequestFields(fields) {
		return fields.reduce((result, field)=>{
			if(field.propertyOf===constant.project.field.propertyOf.MODEL) {
				if(field.operation===constant.project.request.param.operation.UPDATE) {
					result.update.push(field);
				} else {
					result.filter.push(field);
				}
			} else {
				assert.toLog(field.propertyOf===constant.project.field.propertyOf.QUERY);
				result.query.push(field);
			}
			return result;
		}, {
			filter: [],
			query: [],
			update: []
		});
	}

	/**
	 * Configure all of the collections and indices on views that reference our database.
	 * @returns {{id:string, description:string, indices:[Object], name:string}}
	 * @private
	 */
	_createCollections() {
		const compiler=this.openApi.modelCompiler,
			spec=this.openApi.spec,
			models=spec.models.reduce((result, model)=> {
				const views=model.views.filter((view)=>view.serviceId===this.configuration.id);
				if(views.length>0) {
					result.push({
						model,
						views
					});
				}
				return result;
			}, []);
		return models.map(({model, views})=>({
			id: this._modelIdToServiceId(model.id),
			indices: this._createIndices(model, views),
			modelId: model.id,
			name: template.render(model.name, this.openApi.variables),
			schema: compiler.flattenGraph(compiler.modelToGraph(model))
				.map(node=>Object.assign({
					default: node.defaultValue,
					description: node.desc,
					name: node.path
				}, _.omit(node, ["desc", "name", "path"])))
		}));
	}

	/**
	 * Configure all indices for the specified model
	 * @param {PigModel} model
	 * @param {Array<PigView>} views
	 * @return {Array<Object>}
	 * @private
	 */
	_createIndices(model, views) {
		const modelCompiler=new ModelCompiler(this.openApi),
			state={
				indices: [],
				views: views
			};

		/**
		 * This guy tries to be smart and minimizes the number of indices we create.  He does this by
		 * taking advantage of mongo's ability to share indices.  See docs for more info: https://docs.mongodb.com/manual/core/index-compound/
		 * @param {Array<PigView>} _views
		 * @returns {{index: {id:string, name:string, spec:Array<Object<string, number>>}, remaining: [Object]}}
		 */
		function _findIndex(_views) {
			const {viewDatas, fieldCounts}=_views
				.map((view)=>({
					view,
					fields: [],
					filters: modelCompiler.requestFieldsToGraphs(model,
						DatabaseServiceFactory._categorizeRequestFields(view.requestFields).filter
					)
				}))
				.reduce((result, {view, fields, filters})=> {
					// now we are going build each requests <code>fields</code> property and at the same time count
					// the number of references to each field.
					filters.forEach((filter)=> {
						fields.push(filter.path);
						if(result.fieldCounts[filter.path]===undefined) {
							result.fieldCounts[filter.path]=1;
						} else {
							result.fieldCounts[filter.path]++;
						}
					});
					result.viewDatas.push({fields, filters, view});
					return result;
				}, {
					viewDatas: [],
					fieldCounts: {}
				});
			// now that we have totals for all field counts, arrange each field list from most popular to least
			viewDatas.forEach((viewData)=>{
				viewData.fields.sort((f1, f2)=>{
					return fieldCounts[f2.path]-fieldCounts[f1.path]
						|| util.compareStrings(f2.path, f1.path);
				});
			});
			// We select the longest one. Being the longest we know that he cannot be shared (except with an identical index).
			// But those shorter than him may be able to take advantage of a longer one.
			const longest=_.chain(viewDatas)
				.sortBy("fields.length")
				.last()
				.value();
			return {
				index: {
					id: longest.view.id,
					name: longest.fields.join(","),
					spec: longest.fields.map((path)=>({
						// todo: we should figure out which direction makes more sense. But at the moment we don't have direction.
						[path]: 1
					}))
				},
				remaining: viewDatas.reduce((result, viewData)=>{
					if(!_.isEqual(viewData.fields, longest.fields.slice(0, viewData.fields.length))) {
						result.push(viewData.view);
					}
					return result;
				}, [])
			};
		}

		while(state.views.length>0) {
			const {index, remaining}=_findIndex(state.views);
			if(index.spec.length>0) {
				state.indices.push(index);
			}
			state.views=remaining;
		}
		// we reverse it so that the indices go from most basic to most compound. A personal preference.
		return state.indices.reverse();
	}

	/**
	 * Not only is our database treated as a service but each model within is treated like a service by our middleware which
	 * allows operations to be built over individual tables and collections. Here we give it an id that uniquely identifies
	 * this collection as a service within our database.
	 * @param {string} modelId
	 * @returns {string}
	 */
	_modelIdToServiceId(modelId) {
		return modelId.replace(/^.+:/, `urn:db:${this.configuration.type}:col:`);
	}
}

exports.DatabaseServiceFactory=DatabaseServiceFactory;

