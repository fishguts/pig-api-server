/**
 * User: curtis
 * Date: 7/15/18
 * Time: 12:07 AM
 * Copyright @2018 by Xraymen Inc.
 */

const constant=require("../../common/constant");

class HTTPServiceFactory {
	/**
	 * Creates a mongo service definition
	 * @param {OpenApiV2} openApi
	 * @param {Object} configuration
	 */
	constructor(openApi, configuration) {
		this.openApi=openApi;
		this.configuration=configuration;
	}

	/**
	 * Creates an openAPI x-pig-extension configuration
	 * @returns {Object}
	 */
	createServiceConfiguration() {
		// todo: this is just a placeholder
		return {
			id: this.configuration.id,
			class: constant.project.service.proxy.CLASS,
			description: this.configuration.desc,
			name: this.configuration.name,
			type: this.configuration.type,
			request: this.configuration.request,
			response: this.configuration.response
		};
	}

	/**
	 * Translates a view into x-pig.operations
	 * @param {Object} model
	 * @param {Object} view
	 * @returns {[Object]}
	 */
	buildOperations(model, view) {
		this.openApi._addWarning({
			detail: "HTTPServiceFactory.buildOperations() not implemented",
			severity: constant.severity.ERROR,
			subject: "service"
		});
		return [];
	}

	/**
	 * Translates a view parameter into it's x-pig extensions
	 * @param {Object} view
	 * @param {Object} parameter
	 * @returns {Object}
	 */
	buildParameterExtension(view, parameter) {
		this.openApi._addWarning({
			detail: "HTTPServiceFactory.buildParameterExtension() not implemented",
			severity: constant.severity.ERROR,
			subject: "service"
		});
		return [];
	}
}

exports.HTTPServiceFactory=HTTPServiceFactory;
