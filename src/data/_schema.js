/**
 * User: curtis
 * Date: 6/11/18
 * Time: 8:20 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const constant=require("../common/constant");

/**
 * Converts one of our own schemas as created by <link>./_model.js</link> to a formal JSON schema.
 * @param {CompilerGraph} graph
 * @returns {Object}
 */
exports.fromGraph=function(graph) {
	/**
	 * for the most part it is what it should be. We just need to prune some stuff and convert our attributes.
	 * @param {CompilerNode} node
	 * @param {string} property
	 * @param {Object} parent
	 * @returns {Object}
	 * @private
	 */
	function _condition(node, property=null, parent=null) {
		const result=_.pick(node, [
			"$id", "$schema",
			"enum",
			"items",
			"maximum", "minimum",
			"maxItems", "minItems",
			"maxLength", "minLength",
			"pattern", "format",
			"properties",
			"type"
		]);
		// convert our attributes to properties
		if(node.type===constant.project.field.type.OBJECT) {
			// default it and force it to be changed by an attribute
			result.additionalProperties=false;
		}
		_.forEach(node.attributes, (attribute)=> {
			if(attribute===constant.project.field.attribute.MIXED) {
				result.additionalProperties=true;
			} else if(parent && attribute===constant.project.field.attribute.REQUIRED) {
				if(!parent.required) {
					parent.required=[property];
				} else {
					parent.required.push(property);
				}
			}
		});
		exports.makeCompliant(result);
		if(result.type===constant.project.field.type.OBJECT) {
			_.keys(result.properties).forEach((key)=> {
				result.properties[key]=_condition(result.properties[key], key, result);
			});
		} else if(result.type===constant.project.field.type.ARRAY) {
			result.items=_condition(result.items, null, result);
		}
		return result;
	}

	graph=_.cloneDeep(graph);
	return Object.assign(
		{title: graph.name},
		_condition(graph)
	);
};


/**
 * Looks for and does transformations directly on properties that need transforming to be compliant with the JSON schema
 * @param {Object} property
 * @returns {Object} - returns property
 */
exports.makeCompliant=function(property) {
	if(property.desc!==undefined) {
		property.description=property.desc;
		delete property.desc;
	}
	if(property.type===constant.project.field.type.ID) {
		property.type=constant.project.field.type.STRING;
	} else if(property.type===constant.project.field.type.DATE) {
		property.type=constant.project.field.type.STRING;
		property.format="date-time";
	} else if(property.type===constant.project.field.type.ENUM) {
		property.type="string";
		// todo: We are deriving the type from the elements we put in the array but this isn't so hot 'cause our editor will most likely assume that everything is a string.
		if(property.enum) {
			if(_.includes(_.values(constant.project.field.type), typeof(property.enum[0]))) {
				property.type=typeof(property.enum[0]);
			}
		}
	}
	return property;
};

