/**
 * User: curtis
 * Date: 6/3/18
 * Time: 10:17 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const {URL}=require("url");
const _schema=require("./_schema");
const _variable=require("./_variable");
const _util=require("./_util");
const template=require("pig-core").template;
const util=require("pig-core").util;
const constant=require("../common/constant");
const convert=require("../common/convert");
const {ModelCompiler}=require("./_model");
const {ServiceFactory}=require("./_services/factory");

/**
 * It is the brains that takes a project and from it generates and OpenAPI specification.
 * Note: he is surrounded by a local support crew all of which are prefaced with _[\w+].js.
 */
class OpenApiV2 {
	/**
	 * @param {Project} project
	 * @param {string} env
	 */
	constructor(project, env) {
		this._env=env;
		this._project=project;
		this._spec=project.spec;
		/**
		 * Maps schema has to schema
		 * @type {Array<{definition:Object, hash:string, name:string}>}
		 * @private
		 */
		this._schemas=[];
		/**
		 * Compilation of warnings that we encountered building this spec
		 * @type {Array<{subject:"path"|"environment"|"etc", detail:string, severity:"warn"}>}
		 */
		this._warnings=[];
		this._modelCompiler=new ModelCompiler(this);
		this._variables=_variable.toMap({
			env,
			mixins: {env},
			variables: this._spec.variables
		});
		// create schemas for all of our model object
		this._spec.models.forEach((model)=>{
			this._graphToSchema(this._modelCompiler.modelToGraph(model));
		});
	}

	/**
	 * @returns {string}
	 */
	get env() {return this._env;}
	/**
	 * @return {ModelCompiler}
	 */
	get modelCompiler() {
		return this._modelCompiler;
	}
	/**
	 * @returns {PigSpecification}
	 */
	get spec() {return this._spec;}
	/**
	 * @returns {Object<string, string>}
	 */
	get variables() {return this._variables;}

	/**
	 * Factory that converts our project model to the openAPI format
	 * @returns {Object}
	 */
	convert() {
		const result={
			swagger: "2.0",
			basePath: this._buildBasePath(),
			consumes: this._buildConsumes(),
			externalDocs: this._buildExternalDocs(),
			host: this._buildHost(),
			info: this._buildInfo(),
			parameters: this._buildParameters(),
			paths: this._buildPaths(),
			produces: this._buildProduces(),
			responses: this._buildResponses(),
			schemes: this._buildSchemes(),
			securityDefinitions: this._buildSecurityDefinitions(),
			security: this._buildSecurity(),
			tags: this._buildTags(),
			"x-pig": {
				projectId: this._project.id,
				version: this._spec.info.version,
				environment: this._buildEnvironment(),
				log: this._buildLog(),
				model: this._buildModel(),
				services: this._buildServices(),
				warnings: this._buildWarnings()
			}
		};
		// note: these guys are down here because they are dependent on one or more of the functions above.
		result.definitions=this._buildDefinitions();
		// now clean him up and send him home
		return util.scrubObject(result, {
			recursive: true,
			removables: [null, undefined]
		});
	}

	/**** Private Interface ****/
	/**
	 * Adds a warning which will ultimately be encoded in our own extensions
	 * @param {string} detail
	 * @param {string} subject
	 * @param {string} severity
	 * @private
	 */
	_addWarning({detail, subject, severity=constant.severity.WARN}) {
		this._warnings.push({detail, subject, severity});
	}

	/**
	 * @returns {Object}
	 */
	_buildInfo() {
		const info=this._spec.info;
		return {
			description: template.render(info.desc, this._variables),
			license: info.legal.license,
			termsOfService: _.get(info.legal, "terms.url"),
			title: template.render(info.name, this._variables),
			version: template.render(info.version, this._variables)
		};
	}

	/**
	 * Gets the <code>host</code> property which includes the path (or should if there is one)
	 * http://{env}.{project}.com:{port}/v1
	 * @returns {Object}
	 */
	_buildHost() {
		const parsed=new URL(template.render(this._spec.server.url, this._variables));
		// note: <code>host</code> includes the port.  openAPI does not want the protocol
		return parsed.host;
	}

	/**
	 * Our base path is in our host. We pick it out (if there is one)
	 * @returns {Object}
	 */
	_buildBasePath() {
		const parsed=new URL(template.render(this._spec.server.url, this._variables));
		return parsed.pathname;
	}

	/**
	 * Builds our own environment extensions
	 * @returns {Object}
	 */
	_buildEnvironment() {
		const env=_.find(this._spec.envs, {value: this._env});
		if(!env) {
			this._addWarning({
				detail: `could not find an environment for env="${this._env}"`,
				subject: "environment"
			});
			return undefined;
		} else {
			// create a big blob out of him with some of our own defaults
			return {
				debug: env.debug,
				description: env.desc,
				isLower: env.debug,
				name: env.name,
				variables: _variable.getAll(this._spec.variables, this._env)
					.filter((variable)=>variable.scope==="global")
					.reduce((result, variable)=> {
						result[variable.name]=variable.value;
						return result;
					}, {
						DEBUG: env.debug,
						NODE_ENV: this._env
					})
			};
		}
	}

	/**
	 * @returns {Object}
	 */
	_buildExternalDocs() {
		return undefined;
	}

	/**
	 * Builds our own log definition extensions
	 * @returns {Object}
	 */
	_buildLog() {
		const log=_.chain(this._spec.envs)
			.find({value: this._env})
			.get("log")
			.value();
		if(!log) {
			this._addWarning({
				detail: `could not find a log definition for env="${this._env}"`,
				subject: "log"
			});
			return undefined;
		} else {
			return log;
		}
	}

	/**
	 * @returns {[string]}
	 */
	_buildConsumes() {
		return ["application/json"];
	}

	/**
	 * @returns {[string]}
	 */
	_buildProduces() {
		return ["application/json"];
	}

	/**
	 * Builds our own model definition extension or not? Yeah, we are. If nothing else then for reference.
	 * @returns {Object}
	 */
	_buildModel() {
		return this._spec.models.map((model)=>{
			const variables=Object.assign({
				model: template.render(model.name, this._variables)
			}, this._variables);
			return {
				id: model.id,
				description: template.render(model.desc, variables),
				name: template.render(model.name, variables),
				type: model.type,
				fields: _.sortBy(model.fields, "name")
					.map(field=>Object.assign({
						description: template.render(field.desc, variables)
					}, _.omit(field, ["desc"])))
			};
		});
	}

	/**
	 * @returns {Object}
	 */
	_buildSchemes() {
		return [
			"http",
			"https"
		];
	}

	/**
	 * @returns {Object}
	 */
	_buildPaths() {
		const result={};
		_.sortBy(this._project.spec.models, "name").forEach((model)=> {
			_.sortBy(model.views, "path").forEach((view)=> {
				const variables=Object.assign({
						model: template.render(model.name, this._variables),
						method: view.transport.method
					}, this._variables),
					path=template.render(view.path, variables);
				_.merge(result, {
					[path]: this._buildViewPaths({model, view})
				});
			});
		});
		return result;
	}

	/**
	 * @param {PigModel} model
	 * @param {PigView} view
	 * @returns {Object}
	 */
	_buildViewPaths({model, view}) {
		const self=this,
			serviceFactory=new ServiceFactory(this),
			requestGraphs=this._modelCompiler.requestToGraphs(model, view),
			responseGraph=this._modelCompiler.responseToGraph(model, view),
			// descend depth: different databases will have different demands. No-SQL dbs handle objects without a problem. But with relational DBs
			// we emulate the hierarchy with fields. So instead of restricting the depth to which we flatten we track the depth in the data and then
			// the DB services themselves can decide how deep they want to dive when updating objects in a DB.
			flattenedResponseNodes=this._modelCompiler.flattenGraph(responseGraph),
			variables=Object.assign({
				model: template.render(model.name, this._variables),
				method: view.transport.method
			}, this._variables);

		function _buildExtension() {
			return {
				modelId: model.id,
				operations: view.serviceId
					? serviceFactory.buildOperations(model, view)
					: undefined,
				// For the most part The request is covered by params. But literals are not actual params but rather attributes
				// they want applied at runtime as if they were fields.
				request: {
					literals: requestGraphs
						.filter((param)=>param.location===constant.project.request.param.location.LITERAL)
						.map(_buildParameter)
				},
				response: {
					// The schema in the response section is a full blown schema but not field list. Here we break it down by
					// what was explicitly requested.  And then a flattened view (which will probably be more useful to RDBMSs).
					fields: view.responseFields.map(responseField=>{
						const referencedField=_util.referenceFieldToField({model, reference: responseField});
						return {
							default: referencedField.defaultValue,
							id: referencedField.id,
							name: referencedField.name
						};
					}),
					// I am keeping this to the almost bare minimum. One may look up missing properties
					flattened: flattenedResponseNodes.map(node=>({
						default: node.defaultValue,
						id: node.id,
						path: node.path
					}))
				},
				// todo: we don't include the serviceId here because it restricts us. The middleware was designed to be more flexible than we are.
				// https://bitbucket.org/fishguts/pig-api-editor/issues/8/think-about-how-a-service-is-connected-to
				// serviceId: view.serviceId,
				viewId: view.id,
				timeout: view.timeout
			};
		}

		/**
		 * Builds an open-API param out of a pig param
		 * @param {CompilerNode} parameter
		 * @returns {Object}
		 * @private
		 */
		function _buildParameter(parameter) {
			const result={
				default: convert.fromString({
					encoding: parameter.encoding,
					primaryType: parameter.type,
					subType: parameter.subType,
					value: template.render(parameter.defaultValue, variables)
				}),
				description: template.render(parameter.desc, variables),
				enum: parameter.enum,
				in: parameter.location,
				name: parameter.variable || parameter.name,
				pattern: parameter.pattern,
				// we don't have attributes. Let's just use common sense for now
				required: parameter.location!==constant.project.request.param.location.QUERY,
				schema: (parameter.location===constant.project.request.param.location.BODY)
					? {$ref: `#/definitions/${self._graphToSchema(parameter).name}`}
					: undefined,
				type: (parameter.location!==constant.project.request.param.location.BODY)
					? parameter.type
					: undefined,
				"x-pig": serviceFactory.buildParameterExtension(view, parameter)
			};
			return _schema.makeCompliant(result);
		}

		/**
		 * Builds the response codes and associated data one can expect in the response
		 * @returns {{Number: {description: string, schema: Object}}}
		 */
		function _buildResponses() {
			const result={
				200: {
					description: "Successful operation",
					schema: !_.isEmpty(responseGraph)
						? {$ref: `#/definitions/${self._graphToSchema(responseGraph).name}`}
						: undefined
				}
			};
			if(_.includes([
				constant.project.request.function.DELETE_ONE,
				constant.project.request.function.GET_ONE,
				constant.project.request.function.UPDATE_ONE
			], view.function)) {
				result["404"]={
					description: `${_.capitalize(variables.model)} not found`
				};
			}
			if(_.find(requestGraphs, {location: "body"})) {
				result["405"]={
					description: "Validation failed"
				};
			}
			return result;
		}

		return {
			[view.transport.method]: {
				// this is the field that determines the function name. If it is not set then swagger defaults him. THe client
				// is mimic'ing his algorithm so the users should not be surprised
				operationId: _.isEmpty(view.name) ? undefined : view.name,
				schemes: [view.transport.protocol],
				// the openAPI supports both a"description" and a "summary"
				// - summary: A short summary of what the operation does. For maximum readability in the swagger-ui, this field SHOULD be less than 120 characters.
				// - description: A verbose explanation of the operation behavior. GFM syntax can be used for rich text representation.
				// We only capture a single "desc". Summary has more visibility in the generated docs which is why we have chose it.
				summary: template.render(view.desc, variables),
				parameters: requestGraphs
					.filter((parameter)=>parameter.location!==constant.project.request.param.location.LITERAL)
					.map(_buildParameter),
				responses: _buildResponses(),
				tags: [variables.model],
				"x-pig": _buildExtension()
			}
		};
	}

	/**
	 * Builds our own services definition extension
	 * @returns {Object}
	 */
	_buildServices() {
		const factory=new ServiceFactory(this);
		return factory.buildServiceConfigurations();
	}

	/**
	 * All of our stuff is inline. Would make sense to pursue if volume becomes a problem.
	 * @returns {Object}
	 */
	_buildDefinitions() {
		return _.sortBy(this._schemas, "name")
			.reduce((result, schema)=> {
				result[schema.name]=schema.definition;
				return result;
			}, {});
	}

	/**
	 * All of our stuff is inline.
	 * @returns {Object}
	 */
	_buildParameters() {
		return undefined;
	}

	/**
	 * All of our stuff is inline. But this is an interesting idea in terms of consistency and reuse
	 * @returns {Object}
	 */
	_buildResponses() {
		return undefined;
	}

	/**
	 * Nothing for now.
	 * @returns {Object}
	 */
	_buildSecurity() {
		return undefined;
	}

	/**
	 * Would probably be inline.
	 * @returns {Object}
	 */
	_buildSecurityDefinitions() {
		return undefined;
	}

	/**
	 * Puts together an encoding of our current warnings
	 * @returns {Array<string>}
	 * @private
	 */
	_buildWarnings() {
		return _.chain(this._warnings)
			.sortBy(["severity", "subject", "detail"])
			.map((entry)=>`${entry.severity.toUpperCase()}: ${entry.subject} - ${entry.detail}`)
			.value();
	}

	/**
	 * @returns {Object}
	 */
	_buildTags() {
		return _.chain(this._project.spec.models)
			.sortBy("name")
			.map((model)=>({
				description: model.desc,
				name: template.render(model.name, this._variables)
			}))
			.value();
	}

	/**
	 * Converts an internal model representation into a schema and a name.
	 * @param {CompilerGraph} graph
	 * @returns {{definition:Object, name:string}}
	 * @private
	 */
	_graphToSchema(graph) {
		const schema=util.objectToHash(_schema.fromGraph(graph));
		let existing=_.find(this._schemas, {hash: schema.hash});
		if(!existing) {
			// lets find a name for this guy. todo: there is room for improvement
			const root=_.capitalize((graph.type===constant.project.field.type.ARRAY)
				? `${schema.reliable.title}s`
				: schema.reliable.title);
			let name=root;
			for(let index=1; _.find(this._schemas, {name}); index++) {
				name=`${root}.${index}`;
			}
			delete schema.reliable.title;
			this._schemas.push(existing={
				definition: schema.reliable,
				hash: schema.hash,
				name: name
			});
		}
		return existing;
	}
}

exports.OpenApiV2=OpenApiV2;
