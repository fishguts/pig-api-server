/**
 * User: curtis
 * Date: 2/8/18
 * Time: 9:48 PM
 * Copyright @2017 by Xraymen Inc.
 */

const _=require("lodash");
const http=require("pig-core").http;
const {DatabaseCollection}=require("../collection");

const custom={
	options: {
		properties: ["_id"]
	}
};

/**
 * Interface to a mongo collection.
 * @typedef {DatabaseCollection} MongoCollection
 */
class MongoCollection extends DatabaseCollection {
	/**
	 * Sets this collection up for action
	 * @returns {promise}
	 */
	initialize() {
		const configuration=this._configuration;
		return this._connection.database.createCollection(configuration.name, _.omit(configuration.options, custom.options.properties))
			.catch((error)=> {
				return this._createError({
					message: `failed to create mongo collection "${configuration.name}"`
				}, error);
			})
			.then((collection)=> {
				this._collection=collection;
				const indexPromises=_.map(configuration.indices, (index)=> {
					return this._collection.createIndex(index.spec, Object.assign({
						background: true,
						name: index.name
					}, index.options));
				});
				return Promise.all(indexPromises);
			});
	}

	/**** Abstract/Virtual Interface: IO ****/
	/**
	 * Counts the number of documents matching the selector
	 * @param {Object} selector
	 * @returns {Promise<Number>}
	 */
	count(selector) {
		return this._collection.count(selector);
	}

	/**
	 * Inserts 1 or more new documents
	 * @param {[Object]} documents
	 * @returns {Promise}
	 */
	createMany(documents) {
		documents=this._preprocessDocuments(documents);
		return this._collection.insertMany(documents)
			.then((result)=>result.ops)
			.catch((error)=> {
				return (error.code===11000)
					? this._createError({
						ids: _.get(error.message.match(/{\s+:\s+([^}]+)/), 1, "").replace(/"/g, "").trim(),
						message: "id already exists",
						statusCode: http.status.code.CONFLICT
					}, error)
					: error;
			});
	}

	/**
	 * Deletes single document
	 * @param {Object} selector
	 * @returns {Promise}
	 */
	deleteOne(selector) {
		return this._collection.deleteOne(selector)
			.then((result)=> {
				if(result.deletedCount<1) {
					throw this._createError({
						selector: selector,
						statusCode: http.status.code.NOT_FOUND
					});
				} else {
					return result.deletedCount;
				}
			});
	}

	/**
	 * Deletes 0 or more documents
	 * @param {Object} selector
	 * @returns {Promise}
	 */
	deleteMany(selector) {
		return this._collection.deleteMany(selector)
			.then((result)=>(result) ? result.deletedCount : undefined);
	}

	/**
	 * Find 0 or more documents
	 * @param {Object} selector
	 * @param {Object} options
	 * @param {[string]} [options.fields]
	 * @param {Number} [options.skip]
	 * @param {Number} [options.limit]
	 * @param {[string]|string} [options.sort]
	 * @returns {Promise<[Object]>}
	 */
	findMany(selector, options) {
		options=this._preprocessOptions(options);
		return this._collection.find(selector, options).toArray();
	}

	/**
	 * Updates a single document identified by selector
	 * @param {Object} selector
	 * @param {Object} update
	 * @param {Object} options
	 * @param {[string]} [options.fields]
	 * @param {boolean} [options.upsert=false] whether to insert the document if it does not exist
	 * @param {boolean} [options.retrieve=true] whether to insert the document if it does not exist
	 * @returns {Promise<Object>}
	 */
	updateOne(selector, update, options={upsert: false, retrieve: false}) {
		const operations={"$set": this._preprocessDocuments([update])[0]};
		if(options.retrieve) {
			options=this._preprocessOptions(options, true);
			delete options.retrieve;
			options.returnOriginal=false;
			return this._collection.findOneAndUpdate(selector, operations, options)
				.then((result)=> {
					if(result.lastErrorObject.n===0) {
						throw this._createError({
							selector: selector,
							statusCode: http.status.code.NOT_FOUND
						});
					} else {
						return result.value;
					}
				});
		} else {
			return this._collection.updateOne(selector, operations, options)
				.then((result)=>{
					if(result.modifiedCount===0) {
						throw this._createError({
							selector: selector,
							statusCode: http.status.code.NOT_FOUND
						});
					}
					return _.pick(result, ["matchedCount", "modifiedCount"]);
				});
		}
	}

	/**
	 * Updates 0 or more documents identified by selector
	 * @param {Object} selector
	 * @param {Object} update
	 * @param {Object} options
	 * @param {[string]} [options.fields]
	 * @param {boolean} [options.upsert=false] whether to insert the document if it does not exist
	 * @param {boolean} [options.retrieve=true] whether to insert the document if it does not exist
	 * @returns {Promise}
	 */
	updateMany(selector, update, options={upsert: false, retrieve: false}) {
		update=this._preprocessDocuments([update]);
		if(options.retrieve) {
			options=this._preprocessOptions(options);
			delete options.retrieve;
			return this._collection.updateMany(selector, {"$set": update}, options)
				.then(()=>this.findMany(selector, _.pick(options, "fields")));
		} else {
			return this._collection.updateMany(selector, {"$set": update}, options)
				.then((result)=>_.pick(result, ["matchedCount", "modifiedCount"]));
		}
	}

	/**** Private Interface ****/
	/**
	 * Preprocesses documents and applies any rules we may have setup in options to them
	 * @param {[Object]} documents
	 * @returns {[Object]}
	 * @private
	 */
	_preprocessDocuments(documents) {
		if(_.has(this._configuration, "options._id.use")) {
			const use=this._configuration.options._id.use;
			documents=_.map(documents, (document)=>{
				const value=_.get(document, use);
				if(value!=null) {
					document=Object.assign({_id: value}, document);
				}
				return document;
			});
		}
		return documents;
	}

	/**
	 * translates option formats and names into mongo compatible options
	 * @param {Object} options
	 * @param {boolean} [forceClone=false] whether we should clone options even if we don't need to within this function
	 * @returns {Object|undefined}
	 * @private
	 */
	_preprocessOptions(options, forceClone=false) {
		let clone=function() {
			options=Object.assign({}, options);
			clone=()=>options;
			return options;
		};
		if(options) {
			if(forceClone) {
				options=clone();
			}
			if(options.fields || options.sort) {
				options=clone();
				if(options.fields) {
					if(_.isString(options.fields)) {
						options.fields=options.fields.split(/\s*,\s*|\s+/);
					}
					options.projection=options.fields.reduce(function(result, value) {
						if(_.startsWith(value, "-")) {
							result[value.substr(1)]= -1;
						} else {
							result[value]=1;
						}
						return result;
					}, {});
					delete options.fields;
				}
				if(options.sort) {
					if(_.isString(options.sort)) {
						options.sort=options.sort.split(/\s*,\s*|\s+/);
					}
					options.sort=options.sort.map(function(value) {
						return (_.startsWith(value, "-"))
							? [value.substr(1), -1]
							: [value, 1];
					});
				}
			}
		}
		if(_.has(this._configuration, "options._id.hide")) {
			options=clone();
			options.projection=Object.assign({_id: 0}, options.projection);
		}
		return options;
	}
}

exports.MongoCollection=MongoCollection;
