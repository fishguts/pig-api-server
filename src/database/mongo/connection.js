/**
 * User: curtis
 * Date: 2/8/18
 * Time: 9:48 PM
 * Copyright @2017 by Xraymen Inc.
 */


const mongodb=require("mongodb");
const assert=require("pig-core").assert;
const {PigError}=require("pig-core").error;
const log=require("pig-core").log;
const {DatabaseConnection}=require("../connection");

/**
 * Master and commander of a mongo connection.
 * @typedef {DatabaseConnection} MongoConnection
 */
class MongoConnection extends DatabaseConnection {
	/**
	 * @param {Object} configuration
	 */
	constructor(configuration) {
		super(configuration);
		this._client=null;
		this._database=null;
	}

	get database() {
		assert.toLog(this._database, "accessing database before it is not set?");
		return this._database;
	}

	get isConnected() {
		return this._client && this._client.isConnected();
	}

	/**** Implementation of DatabaseConnection functionality ****/
	/**
	 * Clears the database.
	 * @returns {Promise}
	 */
	clear() {
		return this._database.dropDatabase()
			.then(this.initialize.bind(this));
	}
	/**
	 * Sets up this database for action
	 * @returns {promise}
	 */
	initialize() {
		let promise=this.isConnected
			? Promise.resolve()
			: this._connect();
		return promise.then(()=>Promise.all(this.collections.map((collection)=>collection.initialize())));
	}

	/**** Private Interface ****/
	/**
	 * Creates connection to database
	 * @returns {Promise}
	 */
	_connect() {
		return mongodb.MongoClient.connect(this._configuration.server.connectionString, {
			useNewUrlParser: true
		})
			.then((client)=> {
				this._client=client;
				this._database=client.db(this._configuration.name);
				log.verbose(`mongo.connect(): connected to ${this._configuration.server.connectionString}`);
			})
			.catch((error)=> {
				throw new PigError({
					instance: this,
					error: error,
					message: "failed to connect to mongo server"
				});
			});
	}

	/**
	 * Closes the client connection
	 * @returns {promise}
	 */
	_disconnect() {
		if(this._client) {
			const result=this._client.close();
			this._client=null;
			this._database=null;
			return result;
		} else {
			return Promise.resolve();
		}
	}
}

exports.MongoConnection=MongoConnection;
