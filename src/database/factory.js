/**
 * User: curtis
 * Date: 2/8/18
 * Time: 9:48 PM
 * Copyright @2017 by Xraymen Inc.
 */

const _=require("lodash");
const {PigError}=require("pig-core").error;
const {MongoConnection}=require("./mongo/connection");
const {MongoCollection}=require("./mongo/collection");
const constant=require("../common/constant");

const cache={
	connection: {}
};

/**
 * Either gets or creates and gets a connection for the specified configuration.  If the configuration carries
 * and id of a connection we have already loaded then it returns it.  Otherwise it creates one and returns that fellow.
 * @param {Object} configuration
 * @returns {MongoConnection}
 * @throws {Error}
 */
exports.getConnection=function(configuration) {
	if(cache.connection.hasOwnProperty(configuration.id)) {
		return cache.connection[configuration.id];
	}
	switch(configuration.type) {
		case constant.database.type.MONGO: {
			const connection=exports._createMongo(configuration);
			cache.connection[connection.id]=connection;
			return connection;
		}
		default: {
			throw new PigError({
				details: `"${configuration.type}" unknown`,
				message: "unsupported database type"
			});
		}
	}
};

/**
 * Creates and configures a mongo connection as well as all of the collections within
 * @param {Object} configuration
 * @returns {DatabaseConnection}
 * @private
 */
exports._createMongo=function(configuration) {
	const connection=new MongoConnection(configuration);
	connection.collections=_.map(configuration.collections, function(collection) {
		return new MongoCollection(collection, connection);
	});
	return connection;
};
