/**
 * User: curtis
 * Date: 2/18/18
 * Time: 8:09 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");

class DatabaseConnection {
	/**
	 * @param {Object} configuration
	 */
	constructor(configuration) {
		this._configuration=configuration;
		this._collections={};
	}

	/**
	 * A provider such as a database has components such as tables and collections that
	 * may be referenced directly.  It is this provider's job to serve up children if they exist
	 * @returns {[DatabaseCollection]}
	 */
	get collections() { return _.values(this._collections); }
	/**
	 * @param {[DatabaseCollection]} collections
	 */
	set collections(collections) {
		this._collections=_.keyBy(collections, "name");
	}

	/**
	 * Gets the configuration that this provider was built upon
	 * @returns {Object}
	 */
	get configuration() { return this._configuration; }

	/**
	 * Gets the id of this provider
	 * @returns {string}
	 */
	get id() { return this._configuration.id; }

	/**
	 * Is this guy currently connected?
	 * @returns {boolean}
	 * @abstract
	 */
	get isConnected() {
		throw new Error("abstract");
	}

	/**
	 * Gets the name of this provider
	 * @returns {string}
	 */
	get name() { return this._configuration.name; }


	/**** Public API ****/
	/**
	 * Finds collection by name
	 * @param {string} name
	 * @returns {DatabaseCollection}
	 */
	getCollectionByName(name) {
		return this._collections[name];
	}

	/**
	 * Clears the database.
	 * @returns {Promise}
	 * @abstract
	 */
	clear() {
		throw new Error("abstract");
	}

	/**
	 * Initialize this class. Does nothing at this level.
	 * @returns {Promise}
	 */
	initialize() {
		return Promise.resolve();
	}
}

exports.DatabaseConnection=DatabaseConnection;

