/**
 * User: curtis
 * Date: 2/8/18
 * Time: 9:48 PM
 * Copyright @2017 by Xraymen Inc.
 */

const _=require("lodash");
const {PigError}=require("pig-core").error;
const http=require("pig-core").http;

/**
 * Base class for any collection (borrowed mongo's term. Could be thought of as a table)
 *  Note: We are going to try and abstract the implementation. These things don't always go well so
 *  we will allow for a collection to expose the database interface itself but let's try and stay away
 *  from it as our db techs are not set in stone right now and for our ddm layer should be flexible.
 * @abstract
 */
class DatabaseCollection {
	constructor(configuration, connection) {
		this._configuration=configuration;
		this._connection=connection;
	}

	/**
	 * Gets the configuration that this provider was built upon
	 * @returns {Object}
	 */
	get configuration() { return this._configuration; }

	/**
	 * Gets the name of this database
	 * @returns {string}
	 */
	get connection() { return this._connection; }

	/**
	 * Gets the name of the collection as it exists in mongo
	 * @returns {string}
	 */
	get name() { return this._configuration.name; }

	/**** Abstract/Virtual Interface: Initialization ****/
	/**
	 * Chance to initialize this collection. Does nothing by default
	 * @returns {Promise}
	 */
	initialize() {
		return Promise.resolve();
	}

	/**** Abstract/Virtual Interface: IO *****/
	/**
	 * Counts the number of documents matching the selector
	 * @param {Object} selector
	 * @returns {Promise<Number>}
	 * @abstract
	 */
	count(selector) {
		throw new Error("abstract");
	}

	/**
	 * Inserts a new document
	 * @param {Object} document
	 * @returns {Promise<Object>}
	 */
	createOne(document) {
		return this.createMany([document])
			.then((documents)=>_.get(documents, 0));
	}

	/**
	 * Inserts 1 or more new documents
	 * @param {[Object]} documents
	 * @returns {Promise<[Object]>}
	 * @abstract
	 */
	createMany(documents) {
		throw new Error("abstract");
	}

	/**
	 * Deletes single document
	 * @param {Object} selector
	 * @returns {Promise}
	 */
	deleteOne(selector) {
		return this.deleteMany(selector);
	}

	/**
	 * Deletes 0 or more documents
	 * @param {Object} selector
	 * @returns {Promise}
	 * @abstract
	 */
	deleteMany(selector) {
		throw new Error("abstract");
	}

	/**
	 * Find a single document
	 * @param {Object} selector
	 * @param {Object} options
	 * @param {boolean} [options.failNotFound=false]
	 * @param {[string]} [options.fields]
	 * @returns {Promise<Object|null>}
	 */
	findOne(selector, options={failNotFound: true}) {
		return this.findMany(selector, _.omit(options, "failNotFound"))
			.then((documents)=> {
				if(_.size(documents)>0) {
					return documents[0];
				} else if(_.get(options, "failNotFound")) {
					throw this._createError({
						message: `${this.name} document not found`,
						selector: selector,
						statusCode: http.status.code.NOT_FOUND
					});
				} else {
					return null;
				}
			});
	}

	/**
	 * Find 0 or more documents
	 * @param {Object} selector
	 * @param {Object} options
	 * @param {[string]|string} [options.fields]
	 * @param {Number} [options.skip]
	 * @param {Number} [options.limit]
	 * @param {[string]|string} [options.sort]
	 * @returns {Promise<[Object]>}
	 * @abstract
	 */
	findMany(selector, options=undefined) {
		throw new Error("abstract");
	}

	/**
	 * Updates a single document identified by selector
	 * @param {Object} selector
	 * @param {Object} update
	 * @param {Object} options that will be processed locally and by the driver
	 * @param {[string]|string} [options.fields]
	 * @param {boolean} [options.upsert=false] whether to insert the document if it does not exist
	 * @param {boolean} [options.retrieve=true] whether to insert the document if it does not exist
	 * @returns {Promise<Object>}
	 * @abstract
	 */
	updateOne(selector, update, options={upsert: false, retrieve: true}) {
		throw new Error("abstract");
	}

	/**
	 * Updates 0 or more documents identified by selector
	 * @param {Object} selector
	 * @param {Object} update
	 * @param {Object} options that will be processed locally and by the driver
	 * @param {[string]|string} [options.fields]
	 * @param {boolean} [options.upsert=false] whether to insert the document if it does not exist
	 * @param {boolean} [options.retrieve=true] whether to insert the document if it does not exist
	 * @returns {Promise}
	 * @abstract
	 */
	updateMany(selector, update, options={upsert: false, retrieve: true}) {
		throw new Error("abstract");
	}

	/**** Protected Support ****/
	/**
	 * Create a PigError from another error
	 * @param {Object} options
	 * @param {Error} error
	 * @returns {PigError}
	 * @protected
	 */
	_createError(options, error=undefined) {
		if(error) {
			return new PigError(Object.assign({
				instance: this,
				collection: this.name,
				message: error.message,
				error: error
			}, options));
		} else {
			return new PigError(Object.assign({
				instance: this,
				collection: this.name
			}, options));
		}
	}
}

exports.DatabaseCollection=DatabaseCollection;
