/**
 * User: curtis
 * Date: 05/27/18
 * Time: 11:29 PM
 * Copyright @2018 by Xraymen Inc.
 *
 * Application specific commands
 */

const _=require("lodash");
const {Command}=require("pig-cmd").command;
const constant=require("../common/constant");
const {setupEnvironment}=require("../common/setup");
const model_factory=require("../data/factory");

/**
 * Sets up and configures our application.
 */
class SetupEnvironmentCommand extends Command {
	/**
	 * @param {Object} options
	 *  - mode {"cli"|"gui"|"server"} see <code>constant.application.mode</code>
	 */
	constructor(options=undefined) {
		super(Object.assign({
			application: constant.application.mode.SERVER
		}, options));
	}

	execute(callback) {
		const application=model_factory.application.get(_.pick(this.options, ["mode"]));
		setupEnvironment(application);
		process.nextTick(callback);
	}
}

module.exports={
	SetupEnvironmentCommand
};
