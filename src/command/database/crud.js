/**
 * User: curtis
 * Date: 5/29/18
 * Time: 12:14 AM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const {Command}=require("pig-cmd").command;
const db_factory=require("../../database/factory");

/**
 * Base class for all operations.
 * Note: we don't really care at this point if the database is initialized yet. We just need to
 * be able to get the connection and collection. We assume that initialization will be done
 * by the time we execute. If it isn't then something is wrong with our setup.
 * @typedef {Command} MongoCrudCommand
 */
class MongoCrudCommand extends Command {
	/**
	 * @param {Object} configuration - database configuration
	 * @param {string} collectionName - collection name with which we want to work
	 * @param {Object} options - command and mongo options
	 * @param {[string]|string} [options.fields]
	 * @param {Number} [options.skip]
	 * @param {Number} [options.limit]
	 * @param {[string]|string} [options.sort]
	 * @param {boolean} [options.upsert=false] whether to insert the document if it does not exist
	 * @param {boolean} [options.retrieve=true] whether to insert the document if it does not exist
	 */
	constructor({configuration, collectionName, ...options}) {
		super(options);
		this.collection=db_factory
			.getConnection(configuration)
			.getCollectionByName(collectionName);
	}
	/**
	 * Isolates options of interest to mongo
	 * @returns {Object}
	 */
	get mongoOptions() {
		const result=_.pick(this.options, ["fields", "limit", "skip", "retrieve", "sort", "upsert"]);
		// if nothing was specified then let's make sure we get the default params
		return _.isEmpty(result) ? undefined : result;
	}
}

class MongoCreateOneCommand extends MongoCrudCommand {
	constructor({configuration, collectionName, document, ...options}) {
		super({configuration, collectionName, ...options});
		this.document=document;
	}
	execute() {
		return this.collection.createOne(this.document);
	}
}

class MongoCreateManyCommand extends MongoCrudCommand {
	constructor({configuration, collectionName, documents, ...options}) {
		super({configuration, collectionName, ...options});
		this.documents=documents;
	}
	execute() {
		return this.collection.createMany(this.documents);
	}
}

class MongoDeleteOneCommand extends MongoCrudCommand {
	constructor({configuration, collectionName, selector, ...options}) {
		super({configuration, collectionName, ...options});
		this.selector=selector;
	}
	execute() {
		return this.collection.deleteOne(this.selector);
	}
}

class MongoDeleteManyCommand extends MongoCrudCommand {
	constructor({configuration, collectionName, selector, ...options}) {
		super({configuration, collectionName, ...options});
		this.selector=selector;
	}
	execute() {
		return this.collection.deleteMany(this.selector);
	}
}

class MongoFindOneCommand extends MongoCrudCommand {
	constructor({configuration, collectionName, selector, ...options}) {
		super({configuration, collectionName, ...options});
		this.selector=selector;
	}
	execute() {
		return this.collection.findOne(this.selector, this.mongoOptions);
	}
}

class MongoFindManyCommand extends MongoCrudCommand {
	constructor({configuration, collectionName, selector=undefined, ...options}) {
		super({configuration, collectionName, ...options});
		this.selector=selector;
	}
	execute() {
		return this.collection.findMany(this.selector, this.mongoOptions);
	}
}

class MongoUpdateOneCommand extends MongoCrudCommand {
	constructor({configuration, collectionName, selector, data, ...options}) {
		super({configuration, collectionName, ...options});
		this.selector=selector;
		this.data=data;
	}
	execute() {
		return this.collection.updateOne(this.selector, this.data, this.mongoOptions);
	}
}

class MongoUpdateManyCommand extends MongoCrudCommand {
	constructor({configuration, collectionName, selector, data, ...options}) {
		super({configuration, collectionName, ...options});
		this.selector=selector;
		this.data=data;
	}
	execute() {
		return this.collection.updateMany(this.selector, this.data, this.mongoOptions);
	}
}

module.exports={
	MongoCreateOneCommand,
	MongoCreateManyCommand,
	MongoDeleteOneCommand,
	MongoDeleteManyCommand,
	MongoFindOneCommand,
	MongoFindManyCommand,
	MongoUpdateOneCommand,
	MongoUpdateManyCommand
};
