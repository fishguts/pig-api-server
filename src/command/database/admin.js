/**
 * User: curtis
 * Date: 5/28/18
 * Time: 11:26 PM
 * Copyright @2018 by Xraymen Inc.
 */

const {Command}=require("pig-cmd").command;
const db_factory=require("../../database/factory");

/**
 * Sets up the specified database for action.  See any of our <link>settings.yaml</link> for examples
 * of a database configuration.
 * @typedef {Command} SetupDatabaseCommand
 */
class SetupDatabaseCommand extends Command {
	constructor(configuration, options=undefined) {
		super(options);
		this._configuration=configuration;
	}

	execute() {
		const connection=db_factory.getConnection(this._configuration);
		return connection.initialize();
	}
}

/**
 * Clears either the specified database or the default database
 * @typedef {Command} ClearDatabaseCommand
 */
class ClearDatabaseCommand extends Command {
	constructor(configuration, options=undefined) {
		super(options);
		this._configuration=configuration;
	}

	execute() {
		const connection=db_factory.getConnection(this._configuration);
		return connection.clear();
	}
}

module.exports={
	ClearDatabaseCommand,
	SetupDatabaseCommand
};
