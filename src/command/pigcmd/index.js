/**
 * User: curtis
 * Date: 11/19/17
 * Time: 2:19 PM
 * Copyright @2017 by Xraymen Inc.
 *
 * This is the command interface to <link>./src/pigcmd.js</link>
 */

const assert=require("pig-core").assert;
const {Command}=require("pig-cmd").command;

/**
 * All actions supported by our server
 */
exports.ACTIONS={
	"sample": {
		args: "<specify-required>",
		desc: "Describe this command",
		/**
		 * @param {Command} command
		 * @returns {function(callback)}
		 */
		handler: (command)=>command._sampleHandler.bind(command),
		/**
		 * @param {Array<string>} position
		 * @param {Object<string, string>} options
		 * @throws {Error} - if you want to fail validation
		 */
		validate: (position, options)=>{}
	}
};

/**
 * These are a complete list of all of the options we support on a command line:
 * @type {Object<string, PigCliAction>}
 */
exports.OPTIONS=[

];

/**
 * Interface into application specific commands
 * @typedef {Command} CommandInterface
 */
exports.CommandInterface=class CommandInterface extends Command {
	constructor(action, options, position) {
		super(options);
		this.action=action;
		this.position=position;
		this.execute=exports.ACTIONS[this.action].handler(this);
	}

	/**** action handlers ****/
	/**
	 * @param {Function} callback
	 * @private
	 */
	_sampleHandler(callback) {
		assert.ok(false, "implement");
	}
};
