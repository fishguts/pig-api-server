/**
 * User: curtis
 * Date: 6/3/18
 * Time: 7:30 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const async=require("async");
const child_process=require("child_process");
const fs=require("fs-extra");
const jsyaml=require("js-yaml");
const os=require("os");
const path=require("path");
const {PigError}=require("pig-core").error;
const file=require("pig-core").file;
const http=require("pig-core").http;
const template=require("pig-core").template;
const {ProjectCommand}=require("./_base");
const constant=require("../../common/constant");
const {CaptureSwaggerIO}=require("../../common/io");
const data_factory=require("../../data/factory");

/**
 * Given a project this guy will attempt to build it
 */
class ProjectBuildCommand extends ProjectCommand {
	/**
	 * Build this party
	 * @param {Project} project
	 * @param {Function} callback
	 * @private
	 */
	_execute(project, callback) {
		try {
			const specObject=data_factory.openAPI.fromProject(project),
				specTmpPath=path.join(os.tmpdir(), project.id),
				stdioCapture=new CaptureSwaggerIO();
			async.series([
				fs.writeFile.bind(fs, specTmpPath, jsyaml.dump(specObject), {encoding: "utf-8"}),
				this._build.bind(this, project, specTmpPath, stdioCapture),
				this._install.bind(this, project, stdioCapture)
			], (error)=> {
				if(error) {
					const specTargetPath=path.join(project.absBuildPath, "api", "swagger.yaml");
					// we dump the spec to the target directory so that we can research the problem
					file.writeFile(specTargetPath, jsyaml.dump(specObject), {
						async: false,
						createPath: true,
						encoding: "utf8"
					});
					this.log({
						error,
						includeInstanceData: false,
						metadata: {
							id: project.id,
							name: project.name,
							path: specTargetPath,
							spec: specObject
						},
						method: "_execute"
					});
					stdioCapture.dump(`build name="${project.name}", id=${project.id} failed dump`);
					project.build.status=data_factory.status.create({
						error: error,
						value: constant.status.FAILURE,
						stdio: _.pick(stdioCapture, ["err", "out"])
					});
				} else {
					stdioCapture.dump(`build name="${project.name}", id=${project.id} succeeded dump`);
					project.build.status=data_factory.status.create({
						value: constant.status.SUCCESS,
						stdio: _.pick(stdioCapture, ["err", "out"])
					});
				}
				callback(error);
			});
		} catch(error) {
			project.build.status=data_factory.status.create({
				error: error,
				value: constant.status.FAILURE
			});
			callback(error);
		}
	}

	/**
	 * @param {Project} project
	 * @param {string} specPath - path to file containing the openAPI spec we want to build
	 * @param {CaptureStandardIO} stdioCapture
	 * @param {function(error, process)} callback
	 */
	_build(project, specPath, stdioCapture, callback) {
		// when the process exits with errors it is likely that both the "error" event
		// and "close" will be called. We don't want to response to both events.
		callback=_.once(callback);
		// we are going to be changing the cwd. Make us bullet-proof.
		if(!path.isAbsolute(specPath)) {
			specPath=path.resolve(".", specPath);
		}
		const application=data_factory.application.get(),
			commandParams=application.build.params.map((param)=>template.render(param, {
				"inputSpecPath": specPath,
				"outputDirectory": project.absBuildPath
			}));
		this.log({
			includeInstanceData: false,
			message: `running: ${application.build.command} ${commandParams.join(" ")}`,
			method: "_build"
		});
		const process=child_process.spawn(application.build.command, commandParams, {
			cwd: application.build.root
		});
		stdioCapture.attach({emitter: process});
		process.on("error", (error)=> {
			stdioCapture.detach({emitter: process});
			callback(new PigError({
				error,
				statusCode: http.status.code.METHOD_FAILURE
			}));
		});
		process.on("close", (code)=> {
			if(code===0) {
				stdioCapture.detach({emitter: process});
				callback();
			} else {
				// It's going to be very hard to give a meaningful and concise error as to what happened. We will
				// assume that the stdio will do the talking and not a rambling error.
				stdioCapture.detach({emitter: process});
				callback(new PigError({
					message: `Attempt to build "${project.name}" failed`,
					statusCode: http.status.code.METHOD_FAILURE
				}));
			}
		});
	}

	/**
	 * Installs the application which means sets up everything that needs to be done to run it. The reason we
	 * include it in the build is that we want to isolate "run" to running 'cause we will be looking for signature
	 * text (or a signal) to know that he has been setup.  Keeps run simpler.
	 * @param {Project} project
	 * @param {CaptureStandardIO} stdioCapture
	 * @param {Function} callback
	 * @private
	 */
	_install(project, stdioCapture, callback) {
		// when the process exits with errors it is likely that both the "error" event
		// and "close" will be called. We don't want to response to both events.
		callback=_.once(callback);
		const process=child_process.spawn("npm", ["install"], {
			cwd: project.absBuildPath
		});
		stdioCapture.attach({emitter: process});
		process.on("error", (error)=>{
			stdioCapture.detach({emitter: process});
			callback(new PigError({
				error,
				statusCode: http.status.code.METHOD_FAILURE
			}));
		});
		process.on("close", (code)=>{
			if(code===0) {
				stdioCapture.detach({emitter: process});
				callback();
			} else {
				stdioCapture.detach({emitter: process});
				callback(new PigError({
					message: `Attempt to install "${project.name}" failed`,
					statusCode: http.status.code.METHOD_FAILURE
				}));
			}
		});
	}
}

module.exports={
	ProjectBuildCommand
};
