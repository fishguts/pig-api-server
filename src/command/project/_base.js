/**
 * User: curtis
 * Date: 6/3/18
 * Time: 7:30 PM
 * Copyright @2018 by Xraymen Inc.
 */

const assert=require("pig-core").assert;
const {Command}=require("pig-cmd").command;
const data_factory=require("../../data/factory");

/**
 * Base class that does some simple support stuff and makes sure we have a project to work with.
 */
class ProjectCommand extends Command {
	/**
	 * Constructor
	 * @param {string} errorPolicy - the reason we default to warn is that errors are a part of our business
	 * 	and subsequent processing should be designed to handle it.  If you want it to fail then change this default.
	 * @param {Command|class} inputCommand - look for project in specified command
	 * @param {boolean} inputLastCommand - look for project in last command
	 * @param {Project|Object} project
	 * @param {Object} options - optionals
	 */
	constructor({
		errorPolicy="warn",
		inputCommand=undefined,
		inputLastCommand=false,
		project=undefined,
		...options
	}) {
		assert.ok(inputCommand||inputLastCommand||project, "need project or input source for project");
		super({
			errorPolicy,
			inputCommand,
			inputLastCommand,
			inputData: project,
			...options
		});
	}


	execute(callback, history) {
		const project=data_factory.project.ensure(super._getInputData(history)),
			clone=project.clone();
		this._execute(clone, (error)=>{
			// We always want the result but our queue is too smart and will not update the result if there is an error.
			// So set it ourselves and let him do the error handling.
			this.result=clone;
			callback(error);
		});
	}

	/**
	 * Do your business on this project. He is a clone so the original will go unchanged.  This
	 * project has also been plugged in as a result so unless this function results in an error
	 * don't return any params. Rather stick your results in this project.
	 * @param {Project} project
	 * @param {function(error)} callback - only need to callback with errors.
	 * @protected
	 * @abstract
	 */
	_execute(project, callback) {
		throw new Error("abstract");
	}
}

exports.ProjectCommand=ProjectCommand;
