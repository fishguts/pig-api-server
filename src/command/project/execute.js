/**
 * User: curtis
 * Date: 6/3/18
 * Time: 7:30 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const async=require("async");
const child_process=require("child_process");
const fs=require("fs-extra");
const {PigError}=require("pig-core").error;
const http=require("pig-core").http;
const {ProjectCommand}=require("./_base");
const constant=require("../../common/constant");
const {CaptureSwaggerIO}=require("../../common/io");
const _process=require("../../common/process");
const data_factory=require("../../data/factory");


class ProjectRunCommand extends ProjectCommand {
	/**
	 * @param {Project} project
	 * @param {Function} callback
	 * @private
	 */
	_execute(project, callback) {
		const stdioCapture=new CaptureSwaggerIO();
		this._run(project, stdioCapture, (error, subprocess)=> {
			if(error) {
				// note: we log him in debug here 'cause command handling will more properly log him. We just want more detail if we are in debug mode.
				this.log({
					error,
					includeInstanceData: false,
					metadata: {
						id: project.id,
						name: project.name,
						path: project.absBuildPath
					},
					method: "_execute",
					severity: constant.severity.DEBUG
				});
				stdioCapture.dump(`run name="${project.name}", id=${project.id} failed dump`);
				project.run={
					path: project.absBuildPath,
					status: data_factory.status.create({
						error: error,
						value: constant.status.FAILURE,
						stdio: _.pick(stdioCapture, ["err", "out"])
					})
				};
			} else {
				stdioCapture.dump(`run name="${project.name}", id=${project.id} succeeded dump`);
				project.run={
					path: project.absBuildPath,
					pid: subprocess.pid,
					status: data_factory.status.create({
						value: constant.status.SUCCESS,
						stdio: _.pick(stdioCapture, ["err", "out"])
					})
				};
			}
			callback(error);
		});
	}

	/**
	 * @param {Project} project
	 * @param {CaptureStandardIO} stdioCapture
	 * @param {Function} callback
	 * @private
	 */
	_run(project, stdioCapture, callback) {
		const subprocess=child_process.spawn("npm", ["run", "execute"], {
				cwd: project.absBuildPath,
				detached: true,
				shell: true
			}),
			onExit=_.once((code, signal)=>{
				// note: errors should have been logged. This logging is just for debug.
				this.log({
					includeInstanceData: false,
					message: `process closed code=${code}, signal=${signal}`,
					method: "_run"
				});
				if(code===0) {
					stdioCapture.detach({emitter: subprocess});
					callback(new PigError({
						message: `${project.name} unexpectedly exited without an error code`,
						statusCode: http.status.code.METHOD_FAILURE
					}));
				} else {
					// It's going to be very hard to give a meaningful and concise error as to what happened. We will
					// assume that the stdio will do the talking and not a rambling error.
					stdioCapture.detach({emitter: subprocess});
					callback(new PigError({
						message: `Attempt to run "${project.name}" failed`,
						statusCode: http.status.code.METHOD_FAILURE
					}));
				}
			});
		// when the process exits with errors it is likely that both the "error" event
		// and "close" will be called. We don't want to response to both events.
		callback=_.once(callback);
		stdioCapture.attach({emitter: subprocess});
		// this guy is going to run for a while.  We don't hang around waiting for him to end 'cause he might run
		// forever.  But we want to know when he's up and running and can get out of here. So, how do we monitor?
		// Our process is either going to exit with an error or at some point be considered setup.  We are going
		// to monitor the input stream and look for signature text:
		subprocess.on("error", (error)=>{
			this.log({
				message: `process error: code=${error}`,
				method: "_run",
				severity: constant.severity.ERROR
			});
			stdioCapture.detach({emitter: subprocess});
			callback(new PigError({
				error,
				statusCode: http.status.code.METHOD_FAILURE
			}));
		});
		subprocess.on("exit", onExit);
		subprocess.on("close", onExit);
		subprocess.stdout.on("data", (buffer)=>{
			// We are looking for something to indicate that he's up and running:
			// 2018-08-20T02:42:56.210Z INFO: 'testing-extensions' is listening: http://localhost:8080
			// todo: this is sort of hacky. Ideally our child would signal us.  Reasons the following is bad:
			//	1. it's vulnerable to changes
			//	2. log.level may filter out "info" messages
			if(/.* is listening: /.test(buffer.toString())) {
				// let's wait the stdout and stderr buffer to be flushed and then we are done
				process.nextTick(()=>{
					// todo: for some reason when we exit the child process exits. Not too big of a deal, but mysterious
					subprocess.removeAllListeners();
					subprocess.stdout.removeAllListeners();
					subprocess.stderr.removeAllListeners();
					subprocess.unref();
					callback(null, subprocess);
				});
			}
		});
	}
}

/**
 * Will look for this project's pid if there is one.  If nothing needs to be done then he will
 * return the project without changes. If the project's status is updated then it will clone the project
 * and return the new state.
 * Note: this guy does not do any database updating.
 */
class ProjectStatusCommand extends ProjectCommand {
	/**
	 * @param {Project} project
	 * @param {Function} callback
	 * @private
	 */
	_execute(project, callback) {
		async.parallel([
			this._interrogateBuild.bind(this, project),
			this._interrogateRun.bind(this, project)
		], callback);
	}

	_interrogateBuild(project, callback) {
		// if it was in error then it still will be in error
		if(project.build.status.value!==constant.status.SUCCESS) {
			process.nextTick(callback);
		} else {
			// but if it was okay then let's make sure nobody manhandled our project. If he still exists then we assume all is good.
			fs.pathExists(project.absBuildPath, (error, state)=>{
				if(!state || error) {
					// something is wrong. Reset the build status and that is that
					project.build.status=data_factory.status.create();
				}
				callback();
			});
		}
	}

	_interrogateRun(project, callback) {
		if(!project.run.pid) {
			// he's not running. That's all we needed to know. We know that the rest of his status info is up to date.
			process.nextTick(callback);
		} else {
			// here we want to check up on his process and make updates if necessary
			_process.find(project.run.pid, function(error) {
				if(error) {
					if(error.statusCode===http.status.code.NOT_FOUND) {
						// it would seem that our buddy was killed
						project.run={status: data_factory.status.create()};
						project.stop={status: data_factory.status.create()};
						error=undefined;
					} else {
						// hmmmm, this is hard for us to interpret. Think we will reset things with an uncertain status
						project.run=Object.assign(project.run, {
							status: data_factory.status.create({
								value: constant.status.UNKNOWN,
								error: error
							})
						});
						project.stop=Object.assign(project.stop, {
							status: data_factory.status.create({
								value: constant.status.UNKNOWN,
								error: error
							})
						});
					}
				}
				callback(error);
			});
		}
	}
}

/**
 * Will look for and attempt to stop the pid if there is one.  If nothing needs to be done then he will
 * return the project without changes. If the project's status is updated then it will clone the project
 * and return the new state.
 * Note: this guy does not do any database updating.
 */
class ProjectStopCommand extends ProjectCommand {
	/**
	 * @param {Project} project
	 * @param {Function} callback
	 * @private
	 */
	_execute(project, callback) {
		if(!project.run.pid) {
			// he's not running. That's all we needed to know. We know that the rest of
			// his status info is up to date.
			callback(null, project);
		} else {
			// here we want to check up on his process and make updates if necessary
			_process.kill(project.run.pid, function(error) {
				if(!error || error.statusCode===http.status.code.NOT_FOUND) {
					project.run={status: data_factory.status.create()};
					project.stop={status: data_factory.status.create()};
					callback();
				} else {
					project.stop=Object.assign(project.stop, {
						status: data_factory.status.create({
							value: constant.status.FAILURE,
							error: error
						})
					});
					callback(error);
				}
			});
		}
	}
}

module.exports={
	ProjectRunCommand,
	ProjectStatusCommand,
	ProjectStopCommand
};
