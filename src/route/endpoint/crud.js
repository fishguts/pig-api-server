/**
 * User: curtis
 * Date: 05/27/18
 * Time: 9:46 PM
 * Copyright @2018 by Xraymen Inc.
 */

const {Route}=require("pig-cmd").route;
const {FilterInputCommand}=require("pig-cmd").data;
const {
	HttpSendJsonCommand,
	HttpSendStatusCommand
}=require("pig-cmd").http.response;
const {
	MongoCreateOneCommand,
	MongoDeleteOneCommand,
	MongoFindOneCommand,
	MongoFindManyCommand,
	MongoUpdateOneCommand
}=require("../../command/database/crud");
const data_factory=require("../../data/factory");


/**
 * Creates a new project.
 * @typedef {Route} CreateProjectRoute
 */
class CreateProjectRoute extends Route {
	get schema() {
		return "./res/schema/route/metadata-project-create.json#request";
	}

	execute(callback) {
		const application=data_factory.application.get(),
			queue=this.createCommandQueue();
		queue.addCommand(new MongoCreateOneCommand({
			configuration: application.getModuleInfo("mongo"),
			collectionName: "projects",
			document: data_factory.project.createFromSpec(this.req.body).raw
		}));
		queue.addCommand(new HttpSendStatusCommand(this.res));
		queue.execute(callback);
	}
}


/**
 * Deletes a single project
 * @typedef {Route} DeleteProjectRoute
 */
class DeleteProjectRoute extends Route {
	get schema() {
		return "./res/schema/route/metadata-project-delete.json#request";
	}

	execute(callback) {
		const application=data_factory.application.get(),
			queue=this.createCommandQueue();
		queue.addCommand(new MongoDeleteOneCommand({
			configuration: application.getModuleInfo("mongo"),
			collectionName: "projects",
			selector: {_id: this.req.params.id}
		}));
		queue.addCommand(new HttpSendStatusCommand(this.res));
		queue.execute(callback);
	}
}

/**
 * Get a single project
 * @typedef {Route} GetProjectRoute
 */
class GetProjectRoute extends Route {
	get schema() {
		return "./res/schema/route/metadata-project-get.json#request";
	}

	execute(callback) {
		const application=data_factory.application.get(),
			queue=this.createCommandQueue();
		queue.addCommand(new MongoFindOneCommand({
			configuration: application.getModuleInfo("mongo"),
			collectionName: "projects",
			selector: {_id: this.req.params.id},
			fields: "spec"
		}));
		queue.addCommand(new FilterInputCommand({
			inputCommand: MongoFindOneCommand,
			map: (project)=>project.spec
		}));
		queue.addCommand(new HttpSendJsonCommand(this.res, {
			inputCommand: FilterInputCommand
		}));
		queue.execute(callback);
	}
}

/**
 * Updates a single project
 * @typedef {Route} UpdateProjectRoute
 */
class UpdateProjectRoute extends Route {
	get schema() {
		return "./res/schema/route/metadata-project-update.json#request";
	}

	execute(callback) {
		const application=data_factory.application.get(),
			queue=this.createCommandQueue();
		queue.addCommand(new MongoUpdateOneCommand({
			configuration: application.getModuleInfo("mongo"),
			collectionName: "projects",
			data: {spec: this.req.body},
			selector: {_id: this.req.body.id}
		}));
		queue.addCommand(new HttpSendStatusCommand(this.res));
		queue.execute(callback);
	}
}


/**
 * Gets some/all projects
 * @typedef {Route} GetProjectsRoute
 */
class GetProjectsRoute extends Route {
	get schema() {
		return "./res/schema/route/metadata-projects-get.json#request";
	}

	execute(callback) {
		const application=data_factory.application.get(),
			queue=this.createCommandQueue();
		queue.addCommand(new MongoFindManyCommand({
			configuration: application.getModuleInfo("mongo"),
			collectionName: "projects",
			fields: "spec"
		}));
		queue.addCommand(new FilterInputCommand({
			inputCommand: MongoFindManyCommand,
			map: (project)=>project.spec
		}));
		queue.addCommand(new HttpSendJsonCommand(this.res, {
			inputCommand: FilterInputCommand
		}));
		queue.execute(callback);
	}
}

exports.CreateProject=CreateProjectRoute;
exports.DeleteProject=DeleteProjectRoute;
exports.GetProject=GetProjectRoute;
exports.GetProjects=GetProjectsRoute;
exports.UpdateProject=UpdateProjectRoute;
