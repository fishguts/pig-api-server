/**
 * User: curtis
 * Date: 05/27/18
 * Time: 8:31 PM
 * Copyright @2018 by Xraymen Inc.
 */

const http=require("pig-core").http;
const {Route}=require("pig-cmd").route;
const {
	HttpSendJsonCommand,
	HttpSendStatusCommand
}=require("pig-cmd").http.response;
const model_factory=require("../../data/factory");

/**
 * Sends an okay status back to our mother-ship
 * @typedef {Route} PingCommand
 */
class PingCommand extends Route {
	execute(callback) {
		const queue=this.createCommandQueue();
		queue.addCommand(new HttpSendStatusCommand(this.res, {
			statusCode: http.status.code.OK
		}));
		queue.execute(callback);
	}
}

/**
 * Sends configuration of the current environment (with environment info)
 * @typedef {Route} EnvCommand
 */
class EnvCommand extends Route {
	execute(callback) {
		const queue=this.createCommandQueue(),
			application=model_factory.application.get();
		queue.addCommand(new HttpSendJsonCommand(this.res, {
			inputData: application.raw
		}));
		queue.execute(callback);
	}
}

exports.Env=EnvCommand;
exports.Ping=PingCommand;
