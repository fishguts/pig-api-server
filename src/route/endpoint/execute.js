/**
 * User: curtis
 * Date: 05/27/18
 * Time: 9:46 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const http=require("pig-core").http;
const {Route}=require("pig-cmd").route;
const {Command}=require("pig-cmd").command;
const {HttpSendJsonCommand}=require("pig-cmd").http.response;
const {MongoFindOneCommand}=require("../../command/database/crud");
const {ProjectBuildCommand}=require("../../command/project/build");
const {
	ProjectRunCommand,
	ProjectStatusCommand,
	ProjectStopCommand
}=require("../../command/project/execute");
const data_factory=require("../../data/factory");
const db_factory=require("../../database/factory");

/**
 * Builds the specified project and returns the status
 * @typedef {Route} BuildProjectRoute
 */
class BuildProjectRoute extends Route {
	get schema() {
		return "./res/schema/route/metadata-project-build.json#request";
	}

	execute(callback) {
		const application=data_factory.application.get(),
			queue=this.createCommandQueue();
		queue.addCommand(new MongoFindOneCommand({
			configuration: application.getModuleInfo("mongo"),
			collectionName: "projects",
			selector: {_id: this.req.params.id}
		}));
		queue.addCommand(new ProjectBuildCommand({
			inputCommand: MongoFindOneCommand
		}));
		queue.addCommand(new SaveProjectCommand({
			projectExecCommand: ProjectBuildCommand,
			projectLoadCommand: MongoFindOneCommand
		}));
		queue.addCommand(new SendResponseCommand({
			projectExecCommand: ProjectBuildCommand,
			responseDataFilter: (project)=>_.pick(project, ["build"]),
			route: this
		}));
		queue.execute(callback);
	}
}

/**
 * Gets the specified project's status
 * @typedef {Route} GetProjectStatusRoute
 */
class GetProjectStatusRoute extends Route {
	get schema() {
		return "./res/schema/route/metadata-project-status.json#request";
	}

	execute(callback) {
		const application=data_factory.application.get(),
			queue=this.createCommandQueue();
		queue.addCommand(new MongoFindOneCommand({
			configuration: application.getModuleInfo("mongo"),
			collectionName: "projects",
			selector: {_id: this.req.params.id}
		}));
		queue.addCommand(new ProjectStatusCommand({
			inputCommand: MongoFindOneCommand
		}));
		queue.addCommand(new SaveProjectCommand({
			projectExecCommand: ProjectStatusCommand,
			projectLoadCommand: MongoFindOneCommand
		}));
		queue.addCommand(new SendResponseCommand({
			projectExecCommand: ProjectStatusCommand,
			responseDataFilter: (project)=>_.pick(project, ["build", "run", "stop"]),
			route: this
		}));
		queue.execute(callback);
	}
}

/**
 * Runs the specified project
 * @typedef {Route} RunProjectRoute
 */
class RunProjectRoute extends Route {
	get schema() {
		return "./res/schema/route/metadata-project-run.json#request";
	}

	execute(callback) {
		const application=data_factory.application.get(),
			queue=this.createCommandQueue();
		queue.addCommand(new MongoFindOneCommand({
			configuration: application.getModuleInfo("mongo"),
			collectionName: "projects",
			selector: {_id: this.req.params.id}
		}));
		queue.addCommand(new ProjectRunCommand({
			inputCommand: MongoFindOneCommand
		}));
		queue.addCommand(new SaveProjectCommand({
			projectExecCommand: ProjectRunCommand,
			projectLoadCommand: MongoFindOneCommand
		}));
		queue.addCommand(new SendResponseCommand({
			projectExecCommand: ProjectRunCommand,
			responseDataFilter: (project)=>_.pick(project, ["run"]),
			route: this
		}));
		queue.execute(callback);
	}
}

/**
 * Stops the specified project
 * @typedef {Route} StopProjectRoute
 */
class StopProjectRoute extends Route {
	get schema() {
		return "./res/schema/route/metadata-project-stop.json#request";
	}

	execute(callback) {
		const application=data_factory.application.get(),
			queue=this.createCommandQueue();
		queue.addCommand(new MongoFindOneCommand({
			configuration: application.getModuleInfo("mongo"),
			collectionName: "projects",
			selector: {_id: this.req.params.id}
		}));
		queue.addCommand(new ProjectStopCommand({
			inputCommand: MongoFindOneCommand
		}));
		queue.addCommand(new SaveProjectCommand({
			projectExecCommand: ProjectStopCommand,
			projectLoadCommand: MongoFindOneCommand
		}));
		queue.addCommand(new SendResponseCommand({
			projectExecCommand: ProjectStopCommand,
			responseDataFilter: (project)=>_.pick(project, ["stop", "run"]),
			route: this
		}));
		queue.execute(callback);
	}
}

exports.BuildProject=BuildProjectRoute;
exports.GetProjectStatus=GetProjectStatusRoute;
exports.RunProject=RunProjectRoute;
exports.StopProject=StopProjectRoute;


/********************** Private Interface **********************/
/**
 * Compares the project we loaded to the project that was acted upon by our exec command.
 * If exec updated the project then this guy will make sure those changes are persisted.
 * @typedef {Command} SaveProjectCommand
 */
class SaveProjectCommand extends Command {
	constructor({
		projectExecCommand,
		projectLoadCommand,
		...options
	}) {
		super(options);
		this.projectExecCommand=projectExecCommand;
		this.projectLoadCommand=projectLoadCommand;
	}

	execute(callback, history) {
		const projectOriginal=history.lastOfType(this.projectLoadCommand).result,
			projectUpdated=history.lastOfType(this.projectExecCommand).result.raw;
		if(_.isEqual(projectOriginal, projectUpdated)) {
			process.nextTick(callback);
		} else {
			const application=data_factory.application.get(),
				collection=db_factory
					.getConnection(application.getModuleInfo("mongo"))
					.getCollectionByName("projects");
			return collection.updateOne(
				{
					_id: projectOriginal._id
				},
				{
					build: projectUpdated.build,
					stop: projectUpdated.stop,
					run: projectUpdated.run
				}
			);
		}
	}
}

/**
 * This guy puts together our response assuming that we didn't fail irrecoverably which means that
 * we got up to the point of executing our command.  He will look at that command and see what happened
 * and set the statusCode accordingly. And with the calling route's help will make sure to send back
 * the proper subset of project data.
 * @typedef {Command} SendResponseCommand
 */
class SendResponseCommand extends Command {
	/**
	 * @param {class} projectExecCommand
	 * @param {function(project)} responseDataFilter - function that filters the data to include in the response
	 * @param {Route} route
	 * @param {Object} options
	 */
	constructor({
		projectExecCommand,
		responseDataFilter=(project)=>project,
		route,
		...options
	}) {
		super({inputCommand: projectExecCommand, ...options});
		this.projectExecCommand=projectExecCommand;
		this.responseDataFilter=responseDataFilter;
		this.route=route;
	}

	commands(history) {
		const execCommand=history.lastOfType(this.projectExecCommand),
			statusCode=(execCommand.error)
				? _.get(execCommand.error, "statusCode", http.status.code.INTERNAL_SERVER_ERROR)
				: http.status.code.OK;
		return new HttpSendJsonCommand(this.route.res, {
			inputData: this.responseDataFilter(execCommand.result),
			statusCode
		});
	}
}
