/**
 * User: curtis
 * Date: 11/15/17
 * Time: 7:24 PM
 * Copyright @2017 by Xraymen Inc.
 */

const _=require("lodash");
const {Route}=require("pig-cmd").route;
const model_factory=require("../../data/factory");

/**
 * Middleware handler to ensure that the user is authenticated
 * @typedef {Route} AuthenticateRoute
 */
class AuthenticateRoute extends Route {
	execute(callback) {
		if(_.startsWith(this.req.url, "/login")) {
			this.next();
		} else {
			this._authenticate(callback);
		}
	}

	_authenticate(callback) {
		const self=this;
		// todo: this will need to be figured out. It will be his duty to create a user
		// and figure out his permissions. For now we just do the placeholder stuff.
		// note: if he fails authentication then we don't want to call next but want to call 'callback'
		// with auth error (see constant). For now we pretend:
		model_factory.user.create("id")
			.then(function(user) {
				_.set(self.req, "pig.user", user);
				self.next();
			})
			.catch(callback);
	}
}

exports.Authenticate=AuthenticateRoute;
