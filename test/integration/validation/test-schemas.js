/**
 * User: curtis
 * Date: 5/31/18
 * Time: 4:46 PM
 * Copyright @2018 by Xraymen Inc.
 */

const klaw=require("klaw-sync");
const assert=require("pig-core").assert;
const file=require("pig-core").file;
const validator=require("pig-core").validator;

describe("validation.schemas", function() {
	it("should be able to parse all schemas", function() {
		assert.doesNotThrow(validator.validateSchema.bind(validator, "./res/schema/library.json"));
		klaw("./res/schema/route", {nodir: true}).forEach(({path, stats})=>{
			const schema=file.readToJSONSync(path);
			assert.doesNotThrow(validator.validateSchema.bind(validator, schema.request));
			assert.doesNotThrow(validator.validateSchema.bind(validator, schema.response));
		});
	});
});
