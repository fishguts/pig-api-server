/**
 * User: curtis
 * Date: 6/4/18
 * Time: 11:38 PM
 * Copyright @2018 by Xraymen Inc.
 */

const assert=require("pig-core").assert;
const {ProjectBuildCommand}=require("../../../src/command/project/build");
const {CaptureSwaggerIO}=require("../../../src/common/io");
const factory=require("../../support/factory");

describe("command.project.build", function() {
	describe("execute", function() {
		it("should properly convert a pig-spec to an open-api spec and write it to tmp storage", function(done) {
			const project=factory.project.create(),
				command=new ProjectBuildCommand({project});
			command.execute(function(error, process) {
				assert.ifError(error);
				done();
			});
		});
	});

	describe("_build", function() {
		it("given a valid open-api spec", function(done) {
			const project=factory.project.create(),
				command=new ProjectBuildCommand({project}),
				stdioCapture=new CaptureSwaggerIO();
			command._build(project, "./test/data/test-openapi-spec.yaml", stdioCapture, function(error, process) {
				assert.ifError(error);
				stdioCapture.dump();
				done();
			});
		});
	});
});
