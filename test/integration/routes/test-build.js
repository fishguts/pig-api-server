/**
 * User: curtis
 * Date: 6/9/18
 * Time: 11:12 PM
 * Copyright @2018 by Xraymen Inc.
 */

const assert=require("pig-core").assert;
const proxy=require("pig-core").proxy;
const queue=require("pig-cmd").queue;
const request=require("pig-cmd").http.request;
const http=require("../../../src/common/http");
const {SetupTestDatabase, SetupTestProject}=require("../../support/command/database");
const {StartTestServer, StopTestServer}=require("../../support/command/spawn");
const factory=require("../../support/factory");


describe("project.build", function() {
	beforeEach(function(done) {
		proxy.log.stub();
		const series=new queue.SeriesQueue();
		series.addCommand(new SetupTestDatabase());
		series.addCommand(new SetupTestProject());
		series.addCommand(new StartTestServer());
		series.execute(done);
	});

	afterEach(function(done) {
		proxy.log.unstub();
		queue.executeCommand(new StopTestServer(), done);
	});

	it("should successfully build our test project", function(done) {
		const series=new queue.SeriesQueue(),
			url=http.route.project.projectBuild(factory.TEST_PROJECT_ID);
		series.addCommand(new request.HttpPutRequestCommand(url));
		series.execute(function(error, history) {
			assert.ifError(error);
			assert.equal(history.last().result.id, factory.TEST_PROJECT_ID);
			done();
		});
	});

	it("should return a 404 if the project could not be found", function(done) {
		const series=new queue.SeriesQueue(),
			url=http.route.project.ProjectBuildCommand("urn:prj:dne");
		series.addCommand(new request.HttpPutRequestCommand(url));
		series.execute(assert.isError(done));
	});
});


