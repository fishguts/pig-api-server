/**
 * User: curtis
 * Date: 5/31/18
 * Time: 1:46 AM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const assert=require("pig-core").assert;
const proxy=require("pig-core").proxy;
const queue=require("pig-cmd").queue;
const request=require("pig-cmd").http.request;
const http=require("../../../src/common/http");
const {SetupTestDatabase, SetupTestProject}=require("../../support/command/database");
const {StartTestServer, StopTestServer}=require("../../support/command/spawn");
const factory=require("../../support/factory");


describe("project.get", function() {
	beforeEach(function(done) {
		proxy.log.stub();
		const series=new queue.SeriesQueue();
		series.addCommand(new SetupTestDatabase());
		series.addCommand(new SetupTestProject());
		series.addCommand(new StartTestServer());
		series.execute(done);
	});

	afterEach(function(done) {
		proxy.log.unstub();
		queue.executeCommand(new StopTestServer(), done);
	});

	it("should successfully get an existing project", function(done) {
		const series=new queue.SeriesQueue(),
			url=http.route.project.projectGet(factory.TEST_PROJECT_ID);
		series.addCommand(new request.HttpGetRequestCommand(url));
		series.execute(function(error, history) {
			assert.ifError(error);
			assert.equal(history.last().result.id, factory.TEST_PROJECT_ID);
			done();
		});
	});

	it("should return a 404 if the project could not be found", function(done) {
		const series=new queue.SeriesQueue(),
			url=http.route.project.projectGet("urn:prj:dne");
		series.addCommand(new request.HttpGetRequestCommand(url));
		series.execute(assert.isError(done));
	});

	it("should successfully get all projects", function(done) {
		const series=new queue.SeriesQueue(),
			url=http.route.project.projectsGet();
		series.addCommand(new request.HttpGetRequestCommand(url));
		series.execute(function(error, history) {
			assert.ifError(error);
			assert.ok(_.isArray(history.last().result));
			done();
		});
	});
});


