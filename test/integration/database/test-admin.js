/**
 * User: curtis
 * Date: 1/1/18
 * Time: 9:10 PM
 * Copyright @2018 by Xraymen Inc.
 */

const assert=require("pig-core").assert;
const proxy=require("pig-core").proxy;
const queue=require("pig-cmd").queue;
const {SetupTestDatabase, SetupTestProject}=require("../../support/command/database");


describe("database.admin", function() {
	beforeEach(function() {
		proxy.log.stub();
	});

	afterEach(function() {
		proxy.log.unstub();
	});

	it("should properly setup our test database", function(done) {
		const series=new queue.SeriesQueue();
		const setupCommand=new SetupTestDatabase();
		series.addCommand(setupCommand);
		series.execute(function(error) {
			assert.equal(error, null);
			assert.equal(setupCommand.connection.isConnected, true);
			done();
		});
	});

	it("should properly populate our test database", function(done) {
		const series=new queue.SeriesQueue();
		series.addCommand(new SetupTestDatabase());
		series.addCommand(new SetupTestProject());
		series.execute(function(error) {
			assert.equal(error, null);
			done();
		});
	});
});
