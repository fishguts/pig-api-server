/**
* User: curt
* Date: 05/27/18
* Time: 5:48 PM
*/

const assert=require("pig-core").assert;
const queue=require("pig-cmd").queue;
const request=require("pig-cmd").http.request;
const http=require("../../../src/common/http");
const {StartTestServer, StopTestServer}=require("../../support/command/spawn");


describe("workflow.ping", function() {
	it("should successfully spin up a server and ping it and shut it down", function(done) {
		const series=new queue.SeriesQueue(),
			pingUrl=http.route.diag.ping();
		series.addCommand(new StartTestServer({}));
		series.addCommand(new request.HttpGetRequestCommand(pingUrl));
		series.addCommand(new StopTestServer({}));
		series.execute(assert.ifError(done));
	});
});


