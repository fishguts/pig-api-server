/**
* User: curt
* Date: 05/27/18
* Time: 5:45 PM
*/

/* eslint-disable no-console */

const _=require("lodash");
const child_process=require("child_process");
const path=require("path");
const constant=require("../../src/common/constant");

const storage={
	singleton: null
};


/**
 * Spins up a project server for testing
 */
class Server {
	/**
	 * @param {Object} options
	 * 	- verbose {Boolean=false}
	 */
	constructor(options={}) {
		this._options=options;
		this._process=null;
	}

	/**
	 * Gets singleton instance
	 * @param {Object} options
	 * @return {Server}
	 */
	static getSingleton(options={}) {
		if(!storage.singleton) {
			storage.singleton=new Server(options);
		} else {
			// our singleton model breaks down with option differences. To get around this we simply inject this
			// guys options and make sure we react dynamically to option properties
			storage.singleton._options=options;
		}
		return storage.singleton;
	}

	/**
	 * Starts up the server if it is not already started or has been stopped
	 * @param {Function} callback
	 */
	start(callback) {
		if(this._process) {
			// we assume that our tests are synchronous and that nothing will get this far without a successful startup
			process.nextTick(callback);
		} else {
			const self=this,
				logVerbose=function(text) {
					if(self._options.verbose) {
						console.debug(`[spawned-server] ${text}`);
					}
				};

			self._process=child_process.spawn("node", ["./src/server.js"], {
				cwd: path.resolve("."),
				shell: true,
				env: {
					NODE_ENV: constant.nodenv.TEST
				}
			});
			self._process.stdout.on("data", function(data) {
				data=data.toString();
				logVerbose(`${_.trim(data, "\n")}`);
				if(callback && data.match(/server listening on port=\d+/)) {
					callback();
					callback=null;
				}
			});
			self._process.stderr.on("data", function(data) {
				data=data.toString();
				// winston puts DEBUG output on stderr.  If it looks like debug data then ignore it
				if(data.indexOf("DEBUG:")>-1) {
					logVerbose(`${_.trim(data, "\n")}`);
				} else {
					console.error(`[spawned-server] ${_.trim(data, "\n")}`);
					if(callback) {
						self._process.kill("SIGKILL");
						callback(new Error(data));
						callback=null;
					}
				}
			});
			self._process.on("exit", function(code, signal) {
				// if we haven't been killed then send out a warning 'cause he died jim
				if(self._process) {
					console.warn(`[spawned-server] exited code=${code}, signal=${signal}`);
					self._process=null;
				}
			});
		}
	}

	/**
	 * Kill the server if it is running and calls you back after the funeral
	 * @param {Function} callback
	 */
	stop(callback) {
		if(!this._process || this._process.killed) {
			process.nextTick(callback);
		} else {
			this._process.on("exit", _.ary(callback, 1));
			this._process.kill();
			this._process=null;
		}
	}
}

exports.Server=Server;
