/**
 * User: curtis
 * Date: 05/27/18
 * Time: 5:51 PM
 * Copyright @2018 by Xraymen Inc.
 */

const file=require("pig-core").file;
const constant=require("../../src/common/constant");
const model_factory=require("../../src/data/factory");


exports.TEST_PROJECT_ID="urn:prj:test1";
exports.TEST_USER="dummy";

exports.application={
	/**
	 * @returns {Application}
	 */
	get: ()=>model_factory.application.get(constant.nodenv.TEST)
};

exports.project={
	/**
	 * @param {string} path
	 * @returns {Project}
	 */
	create: (path="./test/data/test-project-lean.yaml")=>{
		const project=file.readToJSONSync(path);
		return model_factory.project.create(project);
	}
};
