/**
* User: curt
* Date: 05/27/18
* Time: 5:45 PM
*/

const proxy=require("pig-core").proxy;
const {Command}=require("pig-cmd").command;
const request=require("pig-cmd").http.request;
const factory=require("../factory");
const http=require("../../../src/common/http");
const {Server}=require("../spawn");

/* eslint-disable no-console */

/**
 * Either starts up our singleton instance or looks for an instance and starts up if it cannot find one. See options
 * @typedef {Command} StartTestServerCommand
 */
class StartTestServerCommand extends Command {
	/**
	 * @param {Object} options
	 * - discover {Boolean=true} look for an existing server and use it if one is found
	 * - stubLog {Boolean=true} will stub log locally which means logging from the commands running the tests will be output or not.
	 *     We will exclude error output from the stub being that we allow error policies and errors probably mean errors.
	 * - spawnVerbose {Boolean=false} log spawned server debug std-input or not. Will always log spawned warnings and errors
	 */
	constructor(options=undefined) {
		super(options);
	}

	execute(callback) {
		if(this.getOption("stubLog", true)) {
			proxy.log.stub(["error"]);
		}
		// by default we look for an already, assumed to be, debug server.
		if(!this.getOption("discover", true)) {
			Server.getSingleton(this.options).start(callback);
		} else {
			const pingUrl=http.route.diag.ping();
			this.log({
				message: `in discover mode. Pinging ${factory.application.get().getServerUrl()}`
			});
			new request.HttpGetRequestCommand(pingUrl).execute(error=>{
				// note: we bypass log so that these messages do not get stubbed out
				if(!error) {
					console.info(this.toString(`discovered server already running: ${pingUrl}. NOT starting dedicated integration server`));
					callback();
				} else {
					console.info(this.toString("did not find server. Starting dedicated integration server"));
					Server.getSingleton({
						verbose: this.getOption("spawnVerbose", false)
					}).start(callback);
				}
			});
		}
	}
}

class StopTestServerCommand extends Command {
	execute(callback) {
		const self=this;
		// whether we spawned him or not?  We don't really care. Either we found one and are using it and
		// our local guy isn't running and nothing will happen. Or we started it and the following will stop it.
		Server.getSingleton().stop(error=>{
			if(this.getOption("unstubLog", true)) {
				proxy.log.unstub();
			}
			callback(error);
		});
	}
}


exports.StartTestServer=StartTestServerCommand;
exports.StopTestServer=StopTestServerCommand;
