/**
 * User: curtis
 * Date: 5/30/18
 * Time: 11:58 PM
 * Copyright @2018 by Xraymen Inc.
 */

const file=require("pig-core").file;
const {application}=require("../factory");
const {Command}=require("pig-cmd").command;
const {SetupDatabaseCommand, ClearDatabaseCommand}=require("../../../src/command/database/admin");
const {MongoCreateOneCommand}=require("../../../src/command/database/crud");
const db_factory=require("../../../src/database/factory");

/**
 * This guy connects to our database and then clears it.  Should be called before one sets up our test server.
 * @typedef {Command} SetupTestDatabaseCommand
 */
class SetupTestDatabaseCommand extends Command {
	constructor() {
		super();
		// we keep these guys here for reference by integration tests
		this.configuration=application.get().getModuleInfo("mongo");
		this.connection=db_factory.getConnection(this.configuration);
	}

	commands() {
		return [
			new SetupDatabaseCommand(this.configuration),
			new ClearDatabaseCommand(this.configuration)
		];
	}
}

/**
 * This guy sets up our only test project (at the moment) in our db. See <link>./test/data/test-project-lean.yaml</link>
 * @typedef {Command} SetupTestProjectCommand
 */
class SetupTestProjectCommand extends Command {
	constructor() {
		super();
		// we keep these guys here for reference by integration tests
		this.configuration=application.get().getModuleInfo("mongo");
		this.connection=db_factory.getConnection(this.configuration);
	}

	commands() {
		return [
			new MongoCreateOneCommand({
				configuration: this.configuration,
				collectionName: "projects",
				document: file.readToJSONSync("./test/data/test-project-lean.yaml")
			})
		];
	}
}

exports.SetupTestDatabase=SetupTestDatabaseCommand;
exports.SetupTestProject=SetupTestProjectCommand;
