/**
 * User: curtis
 * Date: 10/14/18
 * Time: 9:09 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const assert=require("pig-core").assert;
const file=require("pig-core").file;
const _util=require("../../../src/data/_util");

describe("data._util", function() {
	/**
	 * @param {string} modelId
	 * @param {string} projectPath
	 * @return {{project: Project, compiler: ModelCompiler, model: PigModel}}
	 */
	function _getModel({
		modelId="urn:mdl:By1EjVlQX",
		projectPath="./test/data/test-project.yaml"
	}={}) {
		const project=file.readToJSONSync(projectPath);
		return {
			project,
			model: _.find(project.spec.models, {id: modelId})
		};
	}

	describe("fieldIdToGamutsField", function() {
		it("should properly find a query field reference", function() {
			const result=_util.fieldIdToGamutsField({id: "urn:mdl:fld:limit"});
			assert.deepEqual(result, {
				"desc": "Limit number of documents in a response",
				"id": "urn:mdl:fld:limit",
				"name": "[limit]",
				"type": "number"
			});
		});

		it("should throw an exception if field cannot be found", function() {
			assert.throws(()=>{
				_util.fieldIdToGamutsField({id: "no:such:field"});
			});
		});
	});

	describe("fieldIdToModelField", function() {
		it("should properly find a model reference", function() {
			const {model}=_getModel(),
				result=_util.fieldIdToModelField({id: "urn:mdl:fld:*", model});
			assert.deepEqual(result, {
				"allowed": {},
				"attributes": [
					"read-only"
				],
				"desc": "Represents the entire model",
				"id": "urn:mdl:fld:*",
				"name": "*",
				"type": "object"
			});
		});

		it("should throw an exception if field cannot be found", function() {
			const {model}=_getModel();
			assert.throws(()=>{
				_util.fieldIdToModelField({id: "no:such:field", model});
			});
		});
	});

	describe("referenceFieldToField", function() {
		it("should properly find a model reference", function() {
			const {model, project}=_getModel(),
				assertImmutable=assert.immutable(project),
				result=_util.referenceFieldToField({
					model, reference: {
						fieldId: "urn:mdl:fld:id",
						propertyOf: "model"
					}
				});
			assert.deepEqual(result, {
				"allowed": {},
				"attributes": [
					"auto-id"
				],
				"desc": "Automatically generated unique ID",
				"id": "urn:mdl:fld:id",
				"name": "_id",
				"type": "id"
			});
			assertImmutable();
		});

		it("should properly find a gamuts reference", function() {
			const {model, project}=_getModel(),
				assertImmutable=assert.immutable(project),
				result=_util.referenceFieldToField({
					model, reference: {
						fieldId: "urn:mdl:fld:limit",
						propertyOf: "query"
					}
				});
			assert.deepEqual(result, {
				"attributes": [
					"required"
				],
				"desc": "Limit number of documents in a response",
				"id": "urn:mdl:fld:limit",
				"name": "limit",
				"type": "number"
			});
			assertImmutable();
		});
	});
});
