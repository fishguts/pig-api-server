/**
 * User: curtis
 * Date: 6/15/18
 * Time: 7:36 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const assert=require("pig-core").assert;
const file=require("pig-core").file;
const validator=require("pig-core").validator;
const _schema=require("../../../src/data/_schema");
const {ModelCompiler}=require("../../../src/data/_model");
const {OpenApiV2}=require("../../../src/data/_openapi");

describe("data._schema", function() {
	/**
	 * @returns {{model: {id:string, fields:[Object], name:string}, project: Project, service: ModelCompiler, view: Object}}
	 * @private
	 */
	function _setupTest({
		modelId="urn:mdl:By1EjVlQX",
		viewId="urn:mdl:vwe:BJe9jNlXX",
		projectPath="./test/data/test-project.yaml"
	}={}) {
		const project=file.readToJSONSync(projectPath),
			openApi=new OpenApiV2(project, "test"),
			model=_.find(project.spec.models, {id: modelId});
		return {
			model,
			project,
			service: new ModelCompiler(openApi),
			view: _.find(model.views, {id: viewId})
		};
	}

	describe("fromGraph", function() {
		it("should properly convert a view request schema", function() {
			const {model, service, view}=_setupTest({viewId: "urn:mdl:vwe:ByVshNlmQ"}),
				requestModels=service.requestToGraphs(model, view),
				assertImmutable=assert.immutable(requestModels[0]),
				jsonSchema=_schema.fromGraph(requestModels[0]);
			assertImmutable();
			assert.equal(validator.validateSchema(Object.assign({$id: "request"}, jsonSchema)), null);
		});

		it("should properly convert a view response schema", function() {
			const {model, service, view}=_setupTest({viewId: "urn:mdl:vwe:ByVshNlmQ"}),
				responseModel=service.responseToGraph(model, view),
				assertImmutable=assert.immutable(responseModel),
				jsonSchema=_schema.fromGraph(responseModel);
			assertImmutable();
			assert.equal(validator.validateSchema(Object.assign({$id: "response"}, jsonSchema)), null);
		});
	});
});
