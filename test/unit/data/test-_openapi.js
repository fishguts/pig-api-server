/**
 * User: curtis
 * Date: 6/13/18
 * Time: 7:47 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const assert=require("pig-core").assert;
const file=require("pig-core").file;
const constant=require("../../../src/common/constant");
const {OpenApiV2}=require("../../../src/data/_openapi");


describe("data._openapi", function() {
	/**
	 *
	 * @param {string} env
	 * @param {string} path
	 * @returns {{instance: OpenApiV2, project: Project}}
	 * @private
	 */
	function _createInstance({
		env=constant.project.env.PIG,
		path="./test/data/test-project.yaml"
	}={}) {
		const project=file.readToJSONSync(path);
		return {
			project,
			instance: new OpenApiV2(project, env)
		};
	}

	describe("_buildInfo", function() {
		it("should properly convert our test project", function() {
			const {instance, project}=_createInstance(),
				result=instance._buildInfo();
			assert.equal(result.description, project.spec.info.desc);
			assert.equal(result.license, project.spec.info.legal.license);
			assert.equal(result.termsOfService, project.spec.info.legal.terms);
			assert.equal(result.title, project.spec.info.name);
			assert.equal(result.version, project.spec.info.version);
		});
	});

	describe("_buildHost", function() {
		it("should properly strip the path from the server url and substitute env variables into template", function() {
			const {instance}=_createInstance(),
				result=instance._buildHost();
			// http://{{env}}.{{project}}.com:{{port}}/v1
			assert.equal(result, "localhost:8080");
		});
	});

	describe("_buildBasePath", function() {
		it("should pull the path from the server url", function() {
			const {instance}=_createInstance(),
				result=instance._buildBasePath();
			// http://{{env}}.{{project}}.com:{{port}}/v1
			assert.equal(result, "/v1");
		});
	});

	describe("_buildConsumes", function() {
		it("should pull back json mime type", function() {
			const {instance}=_createInstance(),
				result=instance._buildConsumes();
			assert.deepEqual(result, ["application/json"]);
		});
	});

	describe("_buildEnvironment", function() {
		it("should create a warning if env cannot be found and return undefined", function() {
			const {instance}=_createInstance({env: "dne"});
			assert.equal(instance._buildEnvironment(), undefined);
			assert.deepEqual(instance._buildWarnings(), [
				"WARN: environment - could not find an environment for env=\"dne\""
			]);
		});

		it("should properly encode a known environment with global variables only", function() {
			const {instance}=_createInstance({env: "pig"});
			assert.deepEqual(instance._buildEnvironment(), {
				"debug": true,
				"description": "pig development environment",
				"isLower": true,
				"name": "pig",
				"variables": {
					"DEBUG": true,
					"NODE_ENV": "pig",
					"GLOBAL": "george"
				}
			});
		});
	});

	describe("_buildProduces", function() {
		it("should pull back json mime type", function() {
			const {instance}=_createInstance(),
				result=instance._buildProduces();
			assert.deepEqual(result, ["application/json"]);
		});
	});

	describe("_buildLog", function() {
		it("should create a warning if env cannot be found and return undefined", function() {
			const {instance}=_createInstance({env: "dne"});
			assert.equal(instance._buildLog(), undefined);
			assert.deepEqual(instance._buildWarnings(), [
				"WARN: log - could not find a log definition for env=\"dne\""
			]);
		});

		it("should properly encode a known environment", function() {
			const {instance}=_createInstance({env: "pig"});
			assert.deepEqual(instance._buildLog(), {
				"level": "debug",
				"transports": [
					{
						"type": "console"
					},
					{
						"type": "file",
						"filename": "pig.log"
					}
				]
			});
		});
	});

	describe("_buildPaths", function() {
		it("properly translate a pig representation of a path into an openapi version", function() {
			const {instance}=_createInstance(),
				result=instance._buildPaths();
			assert.ok(_.has(result, "/user/create.post"));
			assert.ok(_.has(result, "/user/creates.post"));
			assert.ok(_.has(result, "/user/id/{_id}.get"));
			assert.ok(_.has(result, "/user/men.get"));
			assert.ok(_.has(result, "/user/zip/{zip}.get"));
			// let's spot check one path/method
			assert.deepEqual(result["/user/zip/{zip}"].get, {
				"operationId": "userGetByZip",
				"parameters": [
					{
						"description": "User's zip code",
						"in": "path",
						"name": "zip",
						"required": true,
						"type": "integer",
						"x-pig": {
							"attributes": [
								"required"
							],
							"dataPath": "address.zip",
							"flattened": [
								{
									"id": "urn:mdl:fld:Sy9diVemQ",
									"path": "address.zip"
								}
							],
							"function": "filter",
							"id": "urn:mdl:fld:Sy9diVemQ",
							"operation": "gte"
						}
					},
					{
						"default": 50,
						"description": "Limit number of documents in a response",
						"in": "query",
						"name": "limit",
						"required": false,
						"type": "number",
						"x-pig": {
							"dataPath": "limit",
							"function": "query",
							"id": "urn:mdl:fld:limit",
							"operation": "eq"
						}
					}
				],
				"responses": {
					"200": {
						"description": "Successful operation",
						"schema": {
							"$ref": "#/definitions/Users.2"
						}
					}
				},
				"schemes": [
					"http"
				],
				"summary": "Get all users with zip-codes greater than or equal to path param",
				"tags": [
					"user"
				],
				"x-pig": {
					"modelId": "urn:mdl:By1EjVlQX",
					"operations": [
						{
							"serviceId": "urn:db:mongo:col:By1EjVlQX",
							"type": "get-many"
						}
					],
					"request": {
						"literals": []
					},
					"response": {
						"fields": [
							{
								"id": "urn:mdl:fld:ryMUiVx7m",
								"name": "address"
							}
						],
						"flattened": [
							{
								"id": "urn:mdl:fld:BJKvsExXQ",
								"path": "address.city"
							},
							{
								"id": "urn:mdl:fld:Sy9diVemQ",
								"path": "address.zip"
							}
						]
					},
					"viewId": "urn:mdl:vwe:ByVshNlmQ"
				}
			});
		});
	});

	describe("_buildSchemes", function() {
		it("should return undefined", function() {
			const {instance}=_createInstance();
			assert.deepEqual(instance._buildSchemes(), ["http", "https"]);
		});
	});

	describe("_buildServices", function() {
		it("should properly build our one and only database service", function() {
			const {instance}=_createInstance();
			assert.deepEqual(instance._buildServices(), [
				{
					"class": "database",
					"collections": [
						{
							"id": "urn:db:mongo:col:By1EjVlQX",
							"indices": [
								{
									"id": "urn:mdl:vwe:BJe9jNlXX",
									"name": "_id",
									"spec": [
										{
											"_id": 1
										}
									]
								},
								{
									"id": "urn:mdl:vwe:wUALaCEZZ",
									"name": "address.zip",
									"spec": [
										{
											"address.zip": 1
										}
									]
								},
								{
									"id": "urn:mdl:vwe:LDUEMdCAi",
									"name": "type",
									"spec": [
										{
											"type": 1
										}
									]
								}
							],
							"modelId": "urn:mdl:By1EjVlQX",
							"name": "user",
							"schema": [
								{
									"attributes": [
										"auto-id"
									],
									"description": "Automatically generated unique ID",
									"id": "urn:mdl:fld:id",
									"name": "_id",
									"type": "id"
								},
								{
									"attributes": [],
									"description": "User's city",
									"id": "urn:mdl:fld:BJKvsExXQ",
									"name": "address.city",
									"type": "string"
								},
								{
									"attributes": [
										"required"
									],
									"description": "User's zip code",
									"id": "urn:mdl:fld:Sy9diVemQ",
									"name": "address.zip",
									"type": "integer"
								},
								{
									"attributes": [
										"required"
									],
									"description": "User's name",
									"id": "urn:mdl:fld:S1SBs4lQQ",
									"name": "name",
									"pattern": "^\\w{4,16}$",
									"type": "string"
								},
								{
									"attributes": [
										"required"
									],
									"description": "User type",
									"enum": [
										"boy",
										"girl",
										"man",
										"woman"
									],
									"id": "urn:mdl:fld:BJMbU61S7",
									"name": "type",
									"type": "enum"
								}
							]
						}
					],
					"database": {
						"connectionString": "mongodb://localhost:27017",
						"name": "test-mongo"
					},
					"id": "urn:db:mongo:ByW7sNgQm",
					"name": "document storage",
					"type": "mongo"
				}
			]);
		});
	});

	describe("_buildDefinitions", function() {
		it("should build our single 'user' model schema properly", function() {
			const {instance}=_createInstance();
			assert.deepEqual(instance._buildDefinitions(), {
				"User": {
					"additionalProperties": false,
					"properties": {
						"_id": {
							"type": "string"
						},
						"address": {
							"additionalProperties": false,
							"properties": {
								"city": {
									"type": "string"
								},
								"zip": {
									"type": "integer"
								}
							},
							"required": [
								"zip"
							],
							"type": "object"
						},
						"name": {
							"pattern": "^\\w{4,16}$",
							"type": "string"
						},
						"type": {
							"enum": [
								"boy",
								"girl",
								"man",
								"woman"
							],
							"type": "string"
						}
					},
					"required": [
						"address",
						"name",
						"type"
					],
					"type": "object"
				}
			});
		});
	});

	describe("_buildParameters", function() {
		it("should return undefined", function() {
			const {instance}=_createInstance();
			assert.equal(instance._buildParameters(), undefined);
		});
	});

	describe("_buildResponses", function() {
		it("should return undefined", function() {
			const {instance}=_createInstance();
			assert.equal(instance._buildResponses(), undefined);
		});
	});

	describe("_buildSecurity", function() {
		it("should return undefined", function() {
			const {instance}=_createInstance();
			assert.equal(instance._buildSecurity(), undefined);
		});
	});

	describe("_buildSecurityDefinitions", function() {
		it("should return undefined", function() {
			const {instance}=_createInstance();
			assert.equal(instance._buildSecurityDefinitions(), undefined);
		});
	});

	describe("_buildTags", function() {
		it("should return our only model as a tag", function() {
			const {instance}=_createInstance();
			assert.deepEqual(instance._buildTags(), [
				{
					"description": undefined,
					"name": "user"
				}
			]);
		});
	});

	describe("_buildExternalDocs", function() {
		it("should return undefined", function() {
			const {instance}=_createInstance();
			assert.equal(instance._buildExternalDocs(), undefined);
		});
	});

	describe("convert", function() {
		it("should generate an openAPI2.0 compatible spec", function() {
			const {instance}=_createInstance(),
				spec=instance.convert();
			// eslint-disable-next-line no-console
			console.log(JSON.stringify(spec, null, "\t"));
		});
	});
})
;
