/**
 * User: curtis
 * Date: 6/13/18
 * Time: 8:14 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const assert=require("pig-core").assert;
const file=require("pig-core").file;
const constant=require("../../../src/common/constant");
const {ModelCompiler}=require("../../../src/data/_model");
const {OpenApiV2}=require("../../../src/data/_openapi");


describe("data._model", function() {
	/**
	 * @param {string} env
	 * @param {string} modelId
	 * @param {string} projectPath
	 * @return {{project: Project, compiler: ModelCompiler, model: PigModel}}
	 * @private
	 */
	function _createInstance({
		env=constant.project.env.PIG,
		modelId="urn:mdl:By1EjVlQX",
		projectPath="./test/data/test-project.yaml"
	}={}) {
		const project=file.readToJSONSync(projectPath),
			openApi=new OpenApiV2(project, env);
		return {
			project,
			compiler: new ModelCompiler(openApi),
			model: _.find(project.spec.models, {id: modelId})
		};
	}

	describe("_fieldToNode", function() {
		it("should properly convert a simple type", function() {
			const {compiler}=_createInstance();
			assert.deepEqual(compiler._fieldToNode({
				name: "a",
				type: "string"
			}), {
				root: {
					name: "a",
					path: "a",
					type: "string"
				}
			});
		});

		it("should properly assign a deep path's name", function() {
			const {compiler}=_createInstance();
			assert.deepEqual(compiler._fieldToNode({
				name: "a.b",
				type: "string"
			}), {
				root: {
					name: "b",
					path: "a.b",
					type: "string"
				}
			});
		});

		it("should properly mixin 'mixins'", function() {
			const {compiler}=_createInstance();
			assert.deepEqual(compiler._fieldToNode({
				name: "a.b",
				type: "string"
			}, {
				extra: "stuff"
			}), {
				root: {
					extra: "stuff",
					name: "b",
					path: "a.b",
					type: "string"
				}
			});
		});

		it("should properly convert an object type to a node", function() {
			const {compiler}=_createInstance();
			assert.deepEqual(compiler._fieldToNode({
				name: "a",
				type: "object"
			}), {
				root: {
					name: "a",
					path: "a",
					type: "object",
					properties: {}
				},
				leaf: {}
			});
		});

		it("should properly do substitution on the defaultValue", function() {
			const {compiler}=_createInstance();
			assert.deepEqual(compiler._fieldToNode({
				defaultValue: "{{env}}",
				name: "a",
				type: "string"
			}), {
				"root": {
					"name": "a",
					"path": "a",
					"defaultValue": constant.project.env.PIG,
					"type": "string"
				}
			});
		});

		it("should properly convert an array of strings", function() {
			const {compiler}=_createInstance();
			assert.deepEqual(
				compiler._fieldToNode({
					name: "a",
					type: "array",
					subtype: "string"
				}),
				{
					"root": {
						"name": "a",
						"path": "a",
						"type": "array",
						"items": {
							"type": "string"
						}
					},
					"leaf": {
						"type": "string"
					}
				}
			);
		});

		it("should properly convert an array of objects", function() {
			const {compiler}=_createInstance();
			assert.deepEqual(
				compiler._fieldToNode({
					name: "a",
					type: "array",
					subtype: "object"
				}),
				{
					"root": {
						"name": "a",
						"path": "a",
						"type": "array",
						"items": {
							"type": "object",
							"properties": {}
						}
					},
					"leaf": {}
				}
			);
		});

		it("should throw an exception for array of arrays", function() {
			const {compiler}=_createInstance();
			assert.throws(()=>compiler._fieldToNode({
				name: "a",
				type: "array",
				subtype: "array"
			}));
		});
	});

	describe("_fieldToHierarchy", function() {
		it("should properly build out a single tier", function() {
			const {compiler}=_createInstance();
			assert.deepEqual(compiler._fieldToHierarchy({
				modelField: {
					name: "a",
					type: "string"
				},
				model: {}
			}), {
				a: {
					name: "a",
					path: "a",
					type: "string"
				}
			});
		});

		it("should properly build a multiple tier object for an inner tier property", function() {
			const {compiler}=_createInstance();
			assert.deepEqual(compiler._fieldToHierarchy({
				modelField: {
					name: "a.b",
					type: "string"
				},
				model: {
					fields: [{
						type: "object",
						name: "a"
					}]
				}
			}), {
				a: {
					name: "a",
					path: "a",
					type: "object",
					properties: {
						b: {
							name: "b",
							path: "a.b",
							type: "string"
						}
					}
				}
			});
		});

		it("should properly build a multiple tier object with explicit middle tier objects", function() {
			const {compiler}=_createInstance(),
				model={
					fields: [{
						type: "object",
						name: "a"
					}, {
						type: "object",
						name: "a.b"
					}, {
						type: "string",
						name: "a.b.c"
					}]
				};
			const result=compiler._fieldToHierarchy({
				modelField: {
					name: "a",
					type: "object"
				},
				model
			});
			compiler._fieldToHierarchy({
				modelField: {
					name: "a.b",
					type: "object"
				},
				model
			}, result);
			compiler._fieldToHierarchy({
				modelField: {
					name: "a.b.c",
					type: "string"
				},
				model
			}, result);
			assert.deepEqual(result, {
				a: {
					name: "a",
					path: "a",
					type: "object",
					properties: {
						b: {
							name: "b",
							path: "a.b",
							type: "object",
							properties: {
								c: {
									name: "c",
									path: "a.b.c",
									type: "string"
								}
							}
						}
					}
				}
			});
		});

		it("should properly build a multiple tiered array", function() {
			const {compiler}=_createInstance(),
				model={
					fields: [{
						type: "array",
						name: "a"
					}, {
						type: "string",
						name: "a.b"
					}]
				},
				result=compiler._fieldToHierarchy({
					modelField: {
						name: "a",
						type: "array"
					},
					model
				});
			assert.deepEqual(compiler._fieldToHierarchy({
				modelField: {
					name: "a.b",
					type: "string"
				},
				model,
				parent: result
			}), {
				"a": {
					"name": "a",
					"path": "a",
					"type": "array",
					"items": {
						"type": "object",
						"properties": {
							"b": {
								"name": "b",
								"path": "a.b",
								"type": "string"
							}
						}
					}
				}
			});
		});
	});

	describe("requestToGraphs", function() {
		it("should properly assemble a request configuration", function() {
			const {compiler, model}=_createInstance(),
				view=_.find(model.views, {id: "urn:mdl:vwe:BJe9jNlXX"}),
				result=compiler.requestToGraphs(model, view);
			assert.deepEqual(result, [
				{
					"name": "_id",
					"path": "_id",
					"propertyOf": "model",
					"id": "urn:mdl:fld:id",
					"type": "id",
					"desc": "Automatically generated unique ID",
					"attributes": [
						"auto-id"
					],
					"variable": "_id",
					"encoding": "string",
					"location": "path",
					"operation": "eq"
				}
			]);
		});
	});

	describe("responseToGraph", function() {
		it("should properly assemble an empty response configuration", function() {
			const {compiler, model}=_createInstance(),
				view=_.find(model.views, {id: "urn:mdl:vwe:BJGP2EeQm"}),
				result=compiler.responseToGraph(model, view);
			assert.deepEqual(result, {});
		});

		it("should properly assemble a non-empty response configuration", function() {
			const {compiler, model}=_createInstance(),
				view=_.find(model.views, {id: "urn:mdl:vwe:ByVshNlmQ"}),
				result=compiler.responseToGraph(model, view);
			assert.deepEqual(result, {
				"name": "user",
				"path": "user",
				"desc": "user model object",
				"type": "array",
				"items": {
					"type": "object",
					"properties": {
						"address": {
							"name": "address",
							"path": "address",
							"id": "urn:mdl:fld:ryMUiVx7m",
							"attributes": [
								"required"
							],
							"type": "object",
							"desc": "User's address",
							"properties": {
								"city": {
									"name": "city",
									"path": "address.city",
									"id": "urn:mdl:fld:BJKvsExXQ",
									"attributes": [],
									"type": "string",
									"desc": "User's city"
								},
								"zip": {
									"name": "zip",
									"path": "address.zip",
									"id": "urn:mdl:fld:Sy9diVemQ",
									"attributes": [
										"required"
									],
									"type": "integer",
									"desc": "User's zip code"
								}
							}
						}
					}
				}
			});
		});
	});
});
