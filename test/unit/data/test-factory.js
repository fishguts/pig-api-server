/**
 * User: curtis
 * Date: 6/11/18
 * Time: 9:45 PM
 * Copyright @2018 by Xraymen Inc.
 */

const assert=require("pig-core").assert;
const file=require("pig-core").file;
const factory=require("../../../src/data/factory");


describe("data.factory", function() {
	describe("openAPI", function() {
		describe("fromProject", function() {
			it("should properly convert our internal model to the open-api spec", function() {
				const project=file.readToJSONSync("./test/data/test-project.yaml"),
					converted=factory.openAPI.fromProject(project);
				assert.equal(project.spec.info.name, converted.info.title);
			});
		});
	});
});
