/**
 * User: curtis
 * Date: 6/6/18
 * Time: 9:37 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const async=require("async");
const assert=require("pig-core").assert;
const {spawn}=require("child_process");
const _process=require("../../../src/common/process");

describe("common.process", function() {
	describe("ps", function() {
		it("should properly return all processes", function(callback) {
			_process.ps({}, function(error, processes) {
				assert.ifError(error);
				assert.ok(processes.length>0);
				processes.forEach(function(process, index) {
					assert.ok(_.isInteger(process.pid), JSON.stringify(process));
					assert.ok(_.isInteger(process.ppid), JSON.stringify(process));
					assert.ok(_.isString(process.comm), JSON.stringify(process));
					assert.ok(_.isString(process.stat), JSON.stringify(process));
				});
				callback();
			});
		});

		it("should only get child process if requested to do so", function(callback) {
			_process.ps({parentPID: process.pid}, function(error, processes) {
				assert.ifError(error);
				assert.ok(processes.length>0);
				callback();
			});
		});
	});

	describe("find", function() {
		it("should find and kill a process that we create", function(callback) {
			const child=spawn("sleep", ["30"]);
			async.waterfall([
				_process.find.bind(null, child.pid),
				function(process, done) {
					assert.equal(process.pid, child.pid);
					_process.kill(process.pid, done);
				},
				function(done) {
					// make sure she's a goner
					_process.find(child.pid, function(error) {
						assert.equal(error.statusCode, 404);
					});
					done();
				}
			], function(error) {
				assert.ifError(error);
				callback();
			});
		});
	});
});
