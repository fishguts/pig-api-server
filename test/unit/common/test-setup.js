/**
 * User: curtis
 * Date: 2018-12-08
 * Time: 18:40
 * Copyright @2018 by Xraymen Inc.
 */

const assert=require("assert");
const pig_core=require("pig-core");
const pig_cmd=require("pig-cmd");
const proxy=pig_core.proxy;
const test_factory=require("../../support/factory");
const setup=require("../../../src/common/setup");


describe("common.setup", function() {
	afterEach(function() {
		proxy.unstub();
	});

	describe("setupEnvironment", function() {
		it("should properly configure the environment", function() {
			const application=test_factory.application.get();
			proxy.spy(pig_core.log, "configure");
			setup.setupEnvironment(application);
			assert.strictEqual(pig_core.log.configure.callCount, 1);
			assert.strictEqual(pig_cmd.configuration.isLowerEnv, application.isLowerEnv);
			assert.strictEqual(pig_cmd.configuration.name, application.name);
			assert.strictEqual(pig_cmd.configuration.nodenv, application.nodenv);
			assert.strictEqual(pig_cmd.configuration.port, application.server.port);
			assert.strictEqual(pig_cmd.configuration.verbose, application.debug.verbose);
		});

		it("pig-core.log and pig-core.configuration.log should point to same instance", function() {
			assert.strictEqual(pig_core.log, pig_core.configuration.log);
		});
	});
});
