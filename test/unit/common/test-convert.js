/**
 * User: curtis
 * Date: 8/8/18
 * Time: 9:24 PM
 * Copyright @2018 by Xraymen Inc.
 */

const assert=require("pig-core").assert;
const constant=require("../../../src/common/constant");
const convert=require("../../../src/common/convert");


describe("common.convert", function() {
	describe("fromString", function() {
		it("should return undefined for undefined values", function() {
			assert.strictEqual(convert.fromString({
				value: undefined,
				primaryType: constant.project.field.type.NUMBER
			}), undefined);
		});

		it("should return undefined for null values", function() {
			assert.strictEqual(convert.fromString({
				value: null,
				primaryType: constant.project.field.type.NUMBER
			}), undefined);
		});

		[
			constant.project.field.type.DATE,
			constant.project.field.type.ENUM,
			constant.project.field.type.ID,
			constant.project.field.type.STRING
		].forEach((type)=> {
			it(`should return string for values of type ${type}`, function() {
				assert.strictEqual(convert.fromString({
					value: "value",
					primaryType: type
				}), "value");
			});
		});

		it("should properly cast a boolean", function() {
			assert.strictEqual(convert.fromString({
				value: "",
				primaryType: constant.project.field.type.BOOLEAN
			}), undefined);
			assert.strictEqual(convert.fromString({
				value: "true",
				primaryType: constant.project.field.type.BOOLEAN
			}), true);
			assert.strictEqual(convert.fromString({
				value: "True",
				primaryType: constant.project.field.type.BOOLEAN
			}), true);
			assert.strictEqual(convert.fromString({
				value: "1",
				primaryType: constant.project.field.type.BOOLEAN
			}), true);
			assert.strictEqual(convert.fromString({
				value: "false",
				primaryType: constant.project.field.type.BOOLEAN
			}), false);
			assert.strictEqual(convert.fromString({
				value: "0",
				primaryType: constant.project.field.type.BOOLEAN
			}), false);
		});

		it("should properly cast an integer", function() {
			assert.strictEqual(convert.fromString({
				value: "1.1",
				primaryType: constant.project.field.type.INTEGER
			}), 1);
		});

		it("should properly cast a number", function() {
			assert.strictEqual(convert.fromString({
				value: "1.1",
				primaryType: constant.project.field.type.NUMBER
			}), 1.1);
		});

		it("should properly parse and cast an array", function() {
			assert.deepEqual(convert.fromString({
				value: "1,2, 3, 4 , 5.5",
				primaryType: constant.project.field.type.ARRAY,
				subType: constant.project.field.type.INTEGER
			}), [1, 2, 3, 4, 5]);
		});

		it("should properly split a space delimited list", function() {
			assert.deepEqual(convert.fromString({
				encoding: "space-delimited",
				value: "1 2 \t3",
				primaryType: constant.project.field.type.INTEGER
			}), [1, 2, 3]);
		});

		it("should properly split a comma delimited list", function() {
			assert.deepEqual(convert.fromString({
				encoding: "comma-delimited",
				value: "1, 2 , 3",
				primaryType: constant.project.field.type.INTEGER
			}), [1, 2, 3]);
		});

		it("should properly split a semi-colon delimited list", function() {
			assert.deepEqual(convert.fromString({
				encoding: "semi-delimited",
				value: "1; 2 ; 3",
				primaryType: constant.project.field.type.INTEGER
			}), [1, 2, 3]);
		});

		["-", "_", "|", ",", ";", ":"].forEach((delimiter)=> {
			it("should properly split a punctuation delimited list", function() {
				assert.deepEqual(convert.fromString({
					encoding: "punctuation-delimited",
					value: `1${delimiter}2 ${delimiter} 3`,
					primaryType: constant.project.field.type.INTEGER
				}), [1, 2, 3]);
			});
		});

		[
			["comma-delimited", ","],
			["semi-delimited", ";"],
			["punctuation-delimited", "-"]
		].forEach(([encoding, delimeter])=>{
			it("should properly encode nothing embedded in an array string as null", function() {
				assert.deepEqual(convert.fromString({
					encoding: encoding,
					type: constant.project.field.type.ARRAY,
					subType: constant.project.field.type.INTEGER,
					value: `1${delimeter}${delimeter}3`
				}), [1, null, 3]);
			});
		});

		[
			["comma-delimited", ","],
			["semi-delimited", ";"],
			["punctuation-delimited", "-"]
		].forEach(([encoding, delimeter])=>{
			it("should properly encode space embedded in an array string as null", function() {
				assert.deepEqual(convert.fromString({
					encoding: encoding,
					type: constant.project.field.type.ARRAY,
					subType: constant.project.field.type.INTEGER,
					value: `1${delimeter} ${delimeter}3`
				}), [1, null, 3]);
			});
		});
	});
});

