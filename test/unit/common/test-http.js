/**
 * User: curtis
 * Date: 05/27/18
 * Time: 11:50 AM
 * Copyright @2018 by Xraymen Inc.
 */

const assert=require("pig-core").assert;
const http=require("../../../src/common/http");


describe("common.http", function() {
	describe("constructor", function() {
		it("should should assert if missing essentials", function() {
			assert.throws(()=>new http.Url({}));
		});

		it("should should construct properly with pure POJO", function() {
			const options={
				host: "host",
				port: 80
			};
			assert.deepEqual(new http.Url(options).options, options);
		});

		it("should absorb properties of Url object", function() {
			const options={
				url: new http.Url({host: "host"}),
				path: "path"
			};
			assert.deepEqual(new http.Url(options).options, {
				host: "host",
				path: "path"
			});
		});
	});

	describe("build", function() {
		it("should assume a non-secure http protocol", function() {
			const url=new http.Url({
				host: "host",
				port: 80
			});
			assert.equal(url.build(), "http://host:80");
		});

		it("should assume a secure https protocol if port is 443", function() {
			const url=new http.Url({
				host: "host",
				port: 443
			});
			assert.equal(url.build(), "https://host:443");
		});

		it("should join an optional path", function() {
			const url=new http.Url({
				host: "host",
				path: "path",
				port: 80
			});
			assert.equal(url.build(), "http://host:80/path");
		});

		it("should join an optional suffix", function() {
			const url=new http.Url({
				host: "host",
				port: 80
			});
			assert.equal(url.build("suffix"), "http://host:80/suffix");
		});

		it("should join an optional path and suffix", function() {
			const url=new http.Url({
				host: "host",
				path: "path",
				port: 80
			});
			assert.equal(url.build("suffix"), "http://host:80/path/suffix");
		});

		it("cloned url should be build properly", function() {
			const url=new http.Url({
				url: new http.Url({
					host: "host",
					port: 80
				}),
				path: "path"
			});
			assert.equal(url.build(), "http://host:80/path");
		});
	});

	describe("toString", function() {
		it("should forward request to build", function() {
			const url=new http.Url({
				host: "host",
				port: 80
			});
			assert.equal(url.toString(), url.build());
		});
	});
});
