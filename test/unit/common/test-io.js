/**
 * User: curtis
 * Date: 05/27/18
 * Time: 12:35 AM
 * Copyright @2018 by Xraymen Inc.
 */

const assert=require("pig-core").assert;
const {
	CaptureStandardIO,
	EmitStandardIO
}=require("../../../src/common/io");


describe("common.io", function() {
	describe("CaptureStandardIO", function() {
		it("should properly capture stdout", function() {
			const emitter=new EmitStandardIO(),
				capturer=new CaptureStandardIO({emitter});
			emitter.writeOut("test of the emergency broadcast system");
			emitter.close();
			capturer.detach({emitter});
			assert.deepEqual(capturer.out, ["test of the emergency broadcast system"]);
		});

		it("should properly capture stderr", function() {
			const emitter=new EmitStandardIO(),
				capturer=new CaptureStandardIO({emitter});
			emitter.writeError("this is not a test...\n\n");
			emitter.close();
			capturer.detach({emitter});
			assert.deepEqual(capturer.err, ["this is not a test..."]);
		});
	});
});
