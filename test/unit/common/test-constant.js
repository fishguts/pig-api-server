/**
 * User: curtis
 * Date: 05/27/18
 * Time: 8:05 PM
 * Copyright @2018 by Xraymen Inc.
 */

const assert=require("pig-core").assert;
const constant=require("../../../src/common/constant");

describe("common.constant", function() {
	it("should validate module properly", function() {
		assert.equal(constant.isValidModule(constant.module.SERVER), true);
		assert.equal(constant.isValidModule("false"), false);
	});

	it("should validate nodenv properly", function() {
		assert.equal(constant.isValidNodenv(constant.nodenv.DEVELOPMENT), true);
		assert.equal(constant.isValidNodenv("false"), false);
	});

	it("should validate severity properly", function() {
		assert.equal(constant.isValidSeverity(constant.severity.DEBUG), true);
		assert.equal(constant.isValidSeverity("false"), false);
	});
});
